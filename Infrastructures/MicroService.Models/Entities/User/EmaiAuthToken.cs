﻿using System.ComponentModel.DataAnnotations;

namespace MicroService.Models.Entities.BaseManage
{
    /// <summary>
    /// 
    /// </summary>
    public class EmaiAuthToken : BaseEntity<long>
    {
        /// <summary>
        /// Gets or sets the authentication token.
        /// </summary>
        /// <value>
        /// The authentication token.
        /// </value>
        [MaxLength(50)]
        public string AuthToken { get; set; }

        /// <summary>
        /// Gets or sets the authentication token.
        /// </summary>
        /// <value>
        /// The authentication token.
        /// </value>
        [MaxLength(50)]
        public string AuthEmail { get; set; }
    }
}
