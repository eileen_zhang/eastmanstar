﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MicroService.Utils.Extensions
{

    /// <summary>
    /// List Dinstinct Extetion
    /// </summary>
    public static class DinstinctExtetion
    {
        /// <summary>
        /// Distincts the ext.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="C"></typeparam>
        /// <param name="source">The source.</param>
        /// <param name="getfield">The getfield.</param>
        /// <returns></returns>
        public static IEnumerable<T> DistinctExt<T, C>(this IEnumerable<T> source, Func<T, C> getfield) => source.Distinct(new Compare<T, C>(getfield));
    }
}
