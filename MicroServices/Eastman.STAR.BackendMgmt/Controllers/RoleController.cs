﻿using AutoMapper;
using Eastman.STAR.BackendMgmt.Models.Bos;
using Eastman.STAR.BackendMgmt.Models.Dtos;
using Eastman.STAR.BackendMgmt.Services;
using MicroService.BaseService.Controllers;
using MicroService.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Eastman.STAR.BackendMgmt.Controllers
{
    /// <summary>
    /// Role Controller
    /// </summary>
    /// <seealso cref="BaseAPIController" />
    /// <response code="90000">Operate successfully</response>
    /// <response code="90999">Operate failed</response>
    /// <response code="99999">System exception</response>
    [ProducesResponseType(90000),
        ProducesResponseType(90999),
        ProducesResponseType(99999)]
    public class RoleController : BaseAPIController
    {
        private readonly RoleServices _roleService;
        readonly IMapper _mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="RoleController" /> class.
        /// </summary>
        /// <param name="roleService">The role service.</param>
        /// <param name="mapper">The mapper.</param>
        public RoleController(RoleServices roleService, IMapper mapper)
        {
            _roleService = roleService;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets the roles.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ProducesResponseType(typeof(MessageResult<List<RoleOutput>>), 00000)]
        [HttpGet]
        public MessageResult GetRoles([FromQuery] GetRolesInput request)
        {
            try
            {
                var getRolesMapperModel = _mapper.Map<GetRolesBo>(request);
                var responseResult = _roleService.GetRoles(getRolesMapperModel).Result;
                var roleMapperList = new List<RoleOutput>();
                responseResult.ForEach(p => roleMapperList.Add(_mapper.Map<RoleOutput>(p)));
                return MessageResult.SuccessResult(roleMapperList, getRolesMapperModel.TotalCount);
            }
            catch (Exception ex)
            {
                return MessageResult.FailureResult(new MessageModel(9999, ex.Message));
            }
        }

        /// <summary>
        /// Gets the role.
        /// </summary>
        /// <param name="roleId">The role identifier.</param>
        /// <returns></returns>
        [ProducesResponseType(typeof(MessageResult<RoleOutput>), 00000)]
        [HttpGet]
        public MessageResult GetRole(long roleId)
        {
            try
            {
                var responseResult = _roleService.GetRole(roleId).Result;
                var getRoleMapperModel = _mapper.Map<RoleOutput>(responseResult);
                return MessageResult.SuccessResult(getRoleMapperModel);
            }
            catch (Exception ex)
            {
                return MessageResult.FailureResult(new MessageModel(9999, ex.Message));
            }
        }

        /// <summary>
        /// Add Role
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        /// <response code="10007">Id is invalid</response>
        /// <response code="10008">Add or update to be an existed role name</response>
        /// <response code="10009">Role name is empty</response>
        [ProducesResponseType(10007),
            ProducesResponseType(10008),
            ProducesResponseType(10009)]
        [HttpPost]
        public MessageResult AddRole([FromBody] SaveRoleInput request)
        {
            try
            {
                var addRoleMapperModel = _mapper.Map<SaveRoleBo>(request);
                var responseCheck = _roleService.CheckRole(addRoleMapperModel).Result;
                if (!responseCheck.Success)
                {
                    return MessageResult.FailureResult(responseCheck.Message);
                }

                var responseResult = _roleService.AddRole(addRoleMapperModel, CurrentUser.Id).Result;
                return responseResult.Success ? MessageResult.SuccessResult() :
                                       MessageResult.FailureResult(responseResult.Message);
            }
            catch (Exception ex)
            {
                return MessageResult.FailureResult(new MessageModel(9999, ex.Message));
            }
        }

        /// <summary>
        /// Update Role
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        /// <response code="10005">Id or account is changed</response>
        /// <response code="10007">Id is invalid</response>
        /// <response code="10008">Add or update to be an existed role name</response>
        /// <response code="10009">Role name is empty</response>
        [ProducesResponseType(10005),
            ProducesResponseType(10007),
            ProducesResponseType(10008),
            ProducesResponseType(10009)]
        [HttpPost]
        public MessageResult UpdateRole([FromBody] SaveRoleInput request)
        {
            try
            {
                var updateRoleMapperModel = _mapper.Map<SaveRoleBo>(request);
                var responseCheck = _roleService.CheckRole(updateRoleMapperModel).Result;
                if (!responseCheck.Success)
                {
                    return MessageResult.FailureResult(responseCheck.Message);
                }

                var responseResult = _roleService.UpdateRole(updateRoleMapperModel, CurrentUser.Id).Result;
                return responseResult.Success ? MessageResult.SuccessResult() :
                                       MessageResult.FailureResult(responseResult.Message);
            }
            catch (Exception ex)
            {
                return MessageResult.FailureResult(new MessageModel(9999, ex.Message));
            }
        }

        /// <summary>
        /// Deletes the role.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        /// <response code="10007">Id is invalid</response>
        [ProducesResponseType(10007)]
        [HttpPost]
        public MessageResult DeleteRole([FromBody] RoleIdInput request)
        {
            try
            {
                var responseResult = _roleService.DeleteRole(request.RoleId, CurrentUser.Id).Result;
                return responseResult.Success ? MessageResult.SuccessResult() :
                                       MessageResult.FailureResult();
            }
            catch (Exception ex)
            {
                return MessageResult.FailureResult(new MessageModel(9999, ex.Message));
            }
        }
    }
}
