﻿namespace MicroService.Utils.Email
{
    /// <summary>
    /// mail
    /// </summary>
    public class Mail
    {
        /// <summary>
        /// Gets or sets from person.
        /// </summary>
        /// <value>
        /// From person.
        /// </value>
        public string From { get; set; }

        /// <summary>
        /// Gets or sets from person.
        /// </summary>
        /// <value>
        /// From person.
        /// </value>
        public string FromDisplay { get; set; }

        /// <summary>
        /// Gets or sets the recipient arry.
        /// </summary>
        /// <value>
        /// The recipient arry.
        /// </value>
        public string[] RecipientArry { get; set; }

        /// <summary>
        /// Gets or sets the mail cc array.
        /// </summary>
        /// <value>
        /// The mail cc array.
        /// </value>
        public string[] MailCcArray { get; set; }

        /// <summary>
        /// Gets or sets the mail title.
        /// </summary>
        /// <value>
        /// The mail title.
        /// </value>
        public string MailTitle { get; set; }

        /// <summary>
        /// Gets or sets the mail body.
        /// </summary>
        /// <value>
        /// The mail body.
        /// </value>
        public string MailBody { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [isbody HTML].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [isbody HTML]; otherwise, <c>false</c>.
        /// </value>
        public bool IsbodyHtml { get; set; }

    }
}
