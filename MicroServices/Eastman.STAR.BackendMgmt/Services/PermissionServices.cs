﻿using Eastman.STAR.BackendMgmt.Models.Dtos;
using MicroService.Models;
using MicroService.Repository;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eastman.STAR.BackendMgmt.Services
{
    /// <summary>
    /// Permission Services
    /// </summary>
    public class PermissionServices
    {
        readonly IRepository<Permission> _repositoryPermission;
        readonly IRepository<Role> _repositoryRole;

        /// <summary>
        /// Initializes a new instance of the <see cref="PermissionServices"/> class.
        /// </summary>
        /// <param name="repositoryPermission">The repository permission.</param>
        /// <param name="repositoryRole">The repository role.</param>
        public PermissionServices(IRepository<Permission> repositoryPermission, IRepository<Role> repositoryRole)
        {
            _repositoryPermission = repositoryPermission;
            _repositoryRole = repositoryRole;
        }

        /// <summary>
        /// Gets the permission by role identifier.
        /// </summary>
        /// <param name="roleId">The role identifier.</param>
        /// <returns></returns>
        public Task<List<Permission>> GetPermissionByRoleId(long roleId)
        {
            return _repositoryPermission.Entities.Where(a => a.RoleId == roleId).ToListAsync();
        }



        /// <summary>
        /// Gets all permission.
        /// </summary>
        /// <param name="roles">The roles.</param>
        /// <returns></returns>
        public Task<List<Permission>> GetAllPermission(long[] roles)
        {
            return _repositoryPermission.Entities.Where(a => roles.Contains(a.RoleId)).ToListAsync();
        }

        /// <summary>
        /// Save Permission
        /// </summary>
        /// <param name="permissionModel">The permission model.</param>
        /// <returns></returns>
        public async Task<int> SavePermission(PermissionInput permissionModel)
        {
            var roleId = _repositoryRole.Entities.FirstOrDefault(s => s.RoleName == permissionModel.RoleName && !s.IsDeleted).Id;

            var beforePermissions = await _repositoryPermission.Entities.Where(a => a.RoleId == roleId).ToListAsync();

            foreach (var item in beforePermissions)
            {
                item.IsDeleted = true;
                _repositoryPermission.Modify(item);
            }

            foreach (var item in permissionModel.Permissions)
            {
                item.RoleId = roleId;
                _repositoryPermission.Add(item);
            }

            return await _repositoryPermission.SaveChangesAsync();
        }
    }
}
