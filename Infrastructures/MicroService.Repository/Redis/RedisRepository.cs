﻿using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MicroService.Repository
{
    /// <summary>
    /// Redis Repository
    /// </summary>
    public class RedisRepository : IRedisRepository, IDisposable
    {
        readonly IDatabase _db;
        IConnectionMultiplexer _connMultiplexer;

        public RedisRepository(IOptions<RedisOptions> options)
        {
            _connMultiplexer = ConnectionMultiplexer.Connect(options.Value.ConnectionString);
            _db = _connMultiplexer.GetDatabase();
        }

        #region String

        /// <summary>
        /// Sets the string.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <param name="expiry">The expiry.</param>
        /// <returns></returns>
        public bool SetString(RedisKeys key, string value, TimeSpan? expiry = null)
        {
            return _db.StringSet(key.ToString(), value, expiry);
        }

        /// <summary>
        /// Gets the string.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public string GetString(RedisKeys key)
        {
            return _db.StringGet(key.ToString());
        }

        /// <summary>
        /// Sets the string.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <param name="expiry">The expiry.</param>
        /// <returns></returns>
        public bool SetString<T>(RedisKeys key, T value, TimeSpan? expiry = null)
        {
            var json = JsonConvert.SerializeObject(value);
            return _db.StringSet(key.ToString(), json, expiry);
        }

        /// <summary>
        /// Gets the string.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public T GetString<T>(RedisKeys key)
        {
            return JsonConvert.DeserializeObject<T>(_db.StringGet(key.ToString()));
        }

        #endregion String

        #region Hash

        /// <summary>
        /// Determine if the field exists in the hash
        /// </summary>
        /// <param name="key"></param>
        /// <param name="hashField"></param>
        /// <returns></returns>
        public bool HashExists(RedisKeys key, string hashField)
        {
            return _db.HashExists(key.ToString(), hashField);
        }

        /// <summary>
        /// Removes the specified field from the hash
        /// </summary>
        /// <param name="key"></param>
        /// <param name="hashField"></param>
        /// <returns></returns>
        public bool HashDelete(RedisKeys key, string hashField)
        {
            return _db.HashDelete(key.ToString(), hashField);
        }

        /// <summary>
        /// Removes the specified field from the hash
        /// </summary>
        /// <param name="key"></param>
        /// <param name="hashFields"></param>
        /// <returns></returns>
        public long HashDelete(RedisKeys key, IEnumerable<string> hashFields)
        {
            var fields = hashFields.Select(x => (RedisValue)x);
            return _db.HashDelete(key.ToString(), fields.ToArray());
        }

        /// <summary>
        /// Set the value of a field in the hash
        /// </summary>
        /// <param name="key"></param>
        /// <param name="hashField"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool HashSet(RedisKeys key, string hashField, string value)
        {
            return _db.HashSet(key.ToString(), hashField, value);
        }

        /// <summary>
        /// Set the value of a field in the hash
        /// </summary>
        /// <param name="key"></param>
        /// <param name="hashFields"></param>
        public void HashSet(RedisKeys key, IEnumerable<KeyValuePair<string, string>> hashFields)
        {
            var entries = hashFields.Select(x => new HashEntry(x.Key, x.Value));
            _db.HashSet(key.ToString(), entries.ToArray());
        }

        /// <summary>
        /// Get the value in the hash
        /// </summary>
        /// <param name="key"></param>
        /// <param name="hashField"></param>
        /// <returns></returns>
        public string HashGet(RedisKeys key, string hashField)
        {
            return _db.HashGet(key.ToString(), hashField);
        }

        /// <summary>
        /// Get the value in the hash
        /// </summary>
        /// <param name="key"></param>
        /// <param name="hashFields"></param>
        /// <returns></returns>
        public IEnumerable<string> HashGet(RedisKeys key, IEnumerable<string> hashFields)
        {
            var fields = hashFields.Select(x => (RedisValue)x);
            return ConvertStrings(_db.HashGet(key.ToString(), fields.ToArray()));
        }

        /// <summary>
        /// Get the value in the hash
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public IEnumerable<string> HashKeys(RedisKeys key)
        {
            return ConvertStrings(_db.HashKeys(key.ToString()));
        }

        /// <summary>
        /// Get the value in the hash
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public IEnumerable<string> HashValues(RedisKeys key)
        {
            return ConvertStrings(_db.HashValues(key.ToString()));
        }

        /// <summary>
        /// Set the value in the hash
        /// </summary>
        /// <param name="key"></param>
        /// <param name="hashField"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool HashSet<T>(RedisKeys key, string hashField, T value)
        {
            var json = JsonConvert.SerializeObject(value);
            return _db.HashSet(key.ToString(), hashField, json);
        }

        /// <summary>
        /// Set the value from the hash
        /// </summary>
        /// <param name="key"></param>
        /// <param name="hashField"></param>
        /// <returns></returns>
        public T HashGet<T>(RedisKeys key, string hashField)
        {
            return JsonConvert.DeserializeObject<T>(_db.HashGet(key.ToString(), hashField));
        }

        #endregion Hash

        #region List

        /// <summary>
        /// Removes and returns the first element stored in the key list
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string ListLeftPop(RedisKeys key)
        {
            return _db.ListLeftPop(key.ToString());
        }

        /// <summary>
        /// Removes and returns the last element stored in the key list
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string ListRightPop(RedisKeys key)
        {
            return _db.ListRightPop(key.ToString());
        }

        /// <summary>
        /// Removes the element with the same value on the key specified in the list
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public long ListRemove(RedisKeys key, string value)
        {
            return _db.ListRemove(key.ToString(), value);
        }

        /// <summary>
        /// Insert a value at the end of the list.If the key does not exist, create it first and then insert the value
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public long ListRightPush(RedisKeys key, string value)
        {
            return _db.ListRightPush(key.ToString(), value);
        }

        /// <summary>
        /// If the key does not exist, create it first and then insert the value
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public long ListLeftPush(RedisKeys key, string value)
        {
            return _db.ListLeftPush(key.ToString(), value);
        }

        /// <summary>
        /// Returns the length of the key in the list, or 0 if none exists
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public long ListLength(RedisKeys key)
        {
            return _db.ListLength(key.ToString());
        }

        /// <summary>
        /// Returns the element corresponding to the key in the list
        /// </summary>
        /// <param name="key"></param>
        /// <param name="start"></param>
        /// <param name="stop"></param>
        /// <returns></returns>
        public IEnumerable<string> ListRange(RedisKeys key, long start = 0L, long stop = -1L)
        {
            return ConvertStrings(_db.ListRange(key.ToString(), start, stop));
        }

        /// <summary>
        /// Removes and returns the first element stored in the key list
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public T ListLeftPop<T>(RedisKeys key)
        {
            return JsonConvert.DeserializeObject<T>(_db.ListLeftPop(key.ToString()));
        }

        /// <summary>
        /// Removes and returns the last element stored in the key list
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public T ListRightPop<T>(RedisKeys key)
        {
            return JsonConvert.DeserializeObject<T>(_db.ListRightPop(key.ToString()));
        }

        /// <summary>
        /// If the key does not exist, create it first and then insert the value
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public long ListRightPush<T>(RedisKeys key, T value)
        {
            var json = JsonConvert.SerializeObject(value);
            return _db.ListRightPush(key.ToString(), json);
        }

        /// <summary>
        /// Inserts a value at the head of the list.If the key does not exist, create it first and then insert the value
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public long ListLeftPush<T>(RedisKeys key, T value)
        {
            var json = JsonConvert.SerializeObject(value);
            return _db.ListLeftPush(key.ToString(), json);
        }

        #endregion List

        #region SortedSet

        /// <summary>
        /// Add sortedSet
        /// </summary>
        /// <param name="key"></param>
        /// <param name="member"></param>
        /// <param name="score"></param>
        /// <returns></returns>
        public bool SortedSetAdd(RedisKeys key, string member, double score)
        {
            return _db.SortedSetAdd(key.ToString(), member, score);
        }

        /// <summary>
        /// Returns a specified range of elements in an ordered collection, from low to high by default.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="start"></param>
        /// <param name="stop"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        public IEnumerable<string> SortedSetRangeByRank(RedisKeys key, long start = 0L, long stop = -1L, Order order = Order.Ascending)
        {
            return _db.SortedSetRangeByRank(key.ToString(), start, stop, (Order)order).Select(x => x.ToString());
        }

        /// <summary>
        /// Returns the number of elements in the ordered collection
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public long SortedSetLength(RedisKeys key)
        {
            return _db.SortedSetLength(key.ToString());
        }

        /// <summary>
        /// Returns the number of elements in the ordered collection
        /// </summary>
        /// <param name="key"></param>
        /// <param name="memebr"></param>
        /// <returns></returns>
        public bool SortedSetLength(RedisKeys key, string memebr)
        {
            return _db.SortedSetRemove(key.ToString(), memebr);
        }

        /// <summary>
        /// Add sortedSet
        /// </summary>
        /// <param name="key"></param>
        /// <param name="member"></param>
        /// <param name="score"></param>
        /// <returns></returns>
        public bool SortedSetAdd<T>(RedisKeys key, T member, double score)
        {
            var json = JsonConvert.SerializeObject(member);
            return _db.SortedSetAdd(key.ToString(), json, score);
        }

        /// <summary>
        /// The incremental scores sort the members of the set in the store by the key value key by delta
        /// </summary>
        /// <param name="key"></param>
        /// <param name="member"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public double SortedSetIncrement(RedisKeys key, string member, double value = 1)
        {
            return _db.SortedSetIncrement(key.ToString(), member, value);
        }

        #endregion SortedSet

        #region Key

        /// <summary>
        /// Remove the specified key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool KeyDelete(RedisKeys key)
        {
            return _db.KeyDelete(key.ToString());
        }

        /// <summary>
        /// Remove the specified keys
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        public long KeyDelete(IEnumerable<RedisKeys> keys)
        {
            var s = keys.Select(x => (RedisKey)(x.ToString()));
            return _db.KeyDelete(s.ToArray());
        }

        /// <summary>
        /// Verify that the Key exists
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool KeyExists(RedisKeys key)
        {
            return _db.KeyExists(key.ToString());
        }

        /// <summary>
        /// Set the expiration time of the Key
        /// </summary>
        /// <param name="key"></param>
        /// <param name="expiry"></param>
        /// <returns></returns>
        public bool KeyExpire(RedisKeys key, TimeSpan? expiry)
        {
            return _db.KeyExpire(key.ToString(), expiry);
        }

        #endregion key

        #region Private method

        /// <summary>
        /// Converts the strings.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">The list.</param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException">list</exception>
        IEnumerable<string> ConvertStrings<T>(IEnumerable<T> list) where T : struct
        {
            if (list == null)
            {
                throw new ArgumentNullException(nameof(list));
            }
            return list.Select(x => x.ToString());
        }

        public void Dispose()
        {
            _connMultiplexer.Close();
        }

        #endregion private method
    }
}
