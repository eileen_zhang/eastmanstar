﻿namespace Eastman.STAR.BackendMgmt.Models.Dtos
{
    /// <summary>
    /// id input
    /// </summary>
    public class IdInput
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public long Id { get; set; }
    }
}
