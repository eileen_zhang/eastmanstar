﻿using MicroService.Models;

namespace MicroService.BaseService.Models
{
    /// <summary>
    /// base response model
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <seealso cref="" />
    public class BaseResponseBo
    {
        /// <summary>
        /// Interface return value status
        /// </summary>
        /// <value>
        ///   <c>true</c> if success; otherwise, <c>false</c>.
        /// </value>
        public bool Success { get; set; } = true;

        /// <summary>
        /// Return Message Business Model
        /// </summary>
        /// <value>
        /// The message.
        /// </value>
        public MessageModel Message { get; set; }
    }

    /// <summary>
    /// base response model
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <seealso cref="" />
    public class BaseResponseBo<T> where T : class
    {
        /// <summary>
        /// Interface return value status
        /// </summary>
        /// <value>
        ///   <c>true</c> if success; otherwise, <c>false</c>.
        /// </value>
        public bool Success { get; set; } = true;

        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        /// <value>
        /// The data.
        /// </value>
        public T Data { get; set; }

        /// <summary>
        /// Return Message Business Model
        /// </summary>
        /// <value>
        /// The message.
        /// </value>
        public MessageModel Message { get; set; }
    }
}
