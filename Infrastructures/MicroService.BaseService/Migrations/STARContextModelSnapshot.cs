﻿// <auto-generated />
using System;
using MicroService.BaseService;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace MicroService.BaseService.Migrations
{
    [DbContext(typeof(STARContext))]
    partial class STARContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .UseIdentityColumns()
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.0");

            modelBuilder.Entity("MicroService.Models.ConfigDictionary", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .UseIdentityColumn();

                    b.Property<string>("Code")
                        .HasMaxLength(100)
                        .HasColumnType("nvarchar(100)");

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("datetime2");

                    b.Property<long>("CreatedBy")
                        .HasColumnType("bigint");

                    b.Property<DateTime?>("DeletedAt")
                        .HasColumnType("datetime2");

                    b.Property<long?>("DeletedBy")
                        .HasColumnType("bigint");

                    b.Property<string>("Description")
                        .HasMaxLength(500)
                        .HasColumnType("nvarchar(500)");

                    b.Property<int>("DictionaryType")
                        .HasColumnType("int");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("bit");

                    b.Property<DateTime?>("ModifiedAt")
                        .HasColumnType("datetime2");

                    b.Property<long?>("ModifiedBy")
                        .HasColumnType("bigint");

                    b.Property<string>("Name")
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<int>("Order")
                        .HasColumnType("int");

                    b.Property<string>("ParentCode")
                        .HasMaxLength(100)
                        .HasColumnType("nvarchar(100)");

                    b.HasKey("Id");

                    b.ToTable("ConfigDictionarys");

                    b.HasData(
                        new
                        {
                            Id = 1L,
                            Code = "BASIC",
                            CreatedAt = new DateTime(2021, 3, 19, 17, 24, 56, 717, DateTimeKind.Local).AddTicks(6662),
                            CreatedBy = 1L,
                            Description = "default category",
                            DictionaryType = 1001,
                            IsDeleted = false,
                            Name = "基本分类",
                            Order = 0
                        },
                        new
                        {
                            Id = 2L,
                            Code = "AREA",
                            CreatedAt = new DateTime(2021, 3, 19, 17, 24, 56, 717, DateTimeKind.Local).AddTicks(7546),
                            CreatedBy = 1L,
                            Description = "default dictionary about area",
                            DictionaryType = 1002,
                            IsDeleted = false,
                            Name = "地区",
                            Order = 0,
                            ParentCode = "BASIC"
                        },
                        new
                        {
                            Id = 3L,
                            Code = "SEX",
                            CreatedAt = new DateTime(2021, 3, 19, 17, 24, 56, 717, DateTimeKind.Local).AddTicks(7646),
                            CreatedBy = 1L,
                            Description = "default dictionary about sex",
                            DictionaryType = 1002,
                            IsDeleted = false,
                            Name = "性别",
                            Order = 0,
                            ParentCode = "BASIC"
                        },
                        new
                        {
                            Id = 4L,
                            Code = "MALE",
                            CreatedAt = new DateTime(2021, 3, 19, 17, 24, 56, 717, DateTimeKind.Local).AddTicks(7673),
                            CreatedBy = 1L,
                            Description = "default dictionary about sex",
                            DictionaryType = 1003,
                            IsDeleted = false,
                            Name = "男",
                            Order = 0,
                            ParentCode = "SEX"
                        },
                        new
                        {
                            Id = 5L,
                            Code = "FEMALE",
                            CreatedAt = new DateTime(2021, 3, 19, 17, 24, 56, 717, DateTimeKind.Local).AddTicks(7693),
                            CreatedBy = 1L,
                            Description = "default dictionary about sex",
                            DictionaryType = 1003,
                            IsDeleted = false,
                            Name = "女",
                            Order = 0,
                            ParentCode = "SEX"
                        });
                });

            modelBuilder.Entity("MicroService.Models.Entities.BaseManage.EmaiAuthToken", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .UseIdentityColumn();

                    b.Property<string>("AuthEmail")
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("AuthToken")
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("datetime2");

                    b.Property<long>("CreatedBy")
                        .HasColumnType("bigint");

                    b.Property<DateTime?>("DeletedAt")
                        .HasColumnType("datetime2");

                    b.Property<long?>("DeletedBy")
                        .HasColumnType("bigint");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("bit");

                    b.Property<DateTime?>("ModifiedAt")
                        .HasColumnType("datetime2");

                    b.Property<long?>("ModifiedBy")
                        .HasColumnType("bigint");

                    b.HasKey("Id");

                    b.ToTable("EmaiAuthToken");
                });

            modelBuilder.Entity("MicroService.Models.Permission", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .UseIdentityColumn();

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("datetime2");

                    b.Property<long>("CreatedBy")
                        .HasColumnType("bigint");

                    b.Property<DateTime?>("DeletedAt")
                        .HasColumnType("datetime2");

                    b.Property<long?>("DeletedBy")
                        .HasColumnType("bigint");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("bit");

                    b.Property<DateTime?>("ModifiedAt")
                        .HasColumnType("datetime2");

                    b.Property<long?>("ModifiedBy")
                        .HasColumnType("bigint");

                    b.Property<string>("ResourceId")
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<long>("RoleId")
                        .HasColumnType("bigint");

                    b.Property<string>("Type")
                        .HasMaxLength(20)
                        .HasColumnType("nvarchar(20)");

                    b.HasKey("Id");

                    b.ToTable("Permissions");
                });

            modelBuilder.Entity("MicroService.Models.Role", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .UseIdentityColumn();

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("datetime2");

                    b.Property<long>("CreatedBy")
                        .HasColumnType("bigint");

                    b.Property<DateTime?>("DeletedAt")
                        .HasColumnType("datetime2");

                    b.Property<long?>("DeletedBy")
                        .HasColumnType("bigint");

                    b.Property<string>("Description")
                        .HasMaxLength(100)
                        .HasColumnType("nvarchar(100)");

                    b.Property<bool>("IsDefault")
                        .HasColumnType("bit");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("bit");

                    b.Property<DateTime?>("ModifiedAt")
                        .HasColumnType("datetime2");

                    b.Property<long?>("ModifiedBy")
                        .HasColumnType("bigint");

                    b.Property<string>("RoleName")
                        .HasMaxLength(20)
                        .HasColumnType("nvarchar(20)");

                    b.HasKey("Id");

                    b.ToTable("Roles");

                    b.HasData(
                        new
                        {
                            Id = 1L,
                            CreatedAt = new DateTime(2021, 3, 19, 17, 24, 56, 717, DateTimeKind.Local).AddTicks(3593),
                            CreatedBy = 1L,
                            Description = "System default administrator role",
                            IsDefault = true,
                            IsDeleted = false,
                            RoleName = "Administrator"
                        });
                });

            modelBuilder.Entity("MicroService.Models.User", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .UseIdentityColumn();

                    b.Property<string>("Account")
                        .HasMaxLength(32)
                        .HasColumnType("nvarchar(32)");

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("datetime2");

                    b.Property<long>("CreatedBy")
                        .HasColumnType("bigint");

                    b.Property<DateTime?>("DeletedAt")
                        .HasColumnType("datetime2");

                    b.Property<long?>("DeletedBy")
                        .HasColumnType("bigint");

                    b.Property<string>("Email")
                        .HasMaxLength(100)
                        .HasColumnType("nvarchar(100)");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("bit");

                    b.Property<DateTime?>("ModifiedAt")
                        .HasColumnType("datetime2");

                    b.Property<long?>("ModifiedBy")
                        .HasColumnType("bigint");

                    b.Property<string>("Name")
                        .HasMaxLength(255)
                        .HasColumnType("nvarchar(255)");

                    b.Property<long>("OrganizationId")
                        .HasColumnType("bigint");

                    b.Property<string>("Password")
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("Phone")
                        .HasMaxLength(20)
                        .HasColumnType("nvarchar(20)");

                    b.HasKey("Id");

                    b.ToTable("Users");

                    b.HasData(
                        new
                        {
                            Id = 1L,
                            Account = "admin",
                            CreatedAt = new DateTime(2021, 3, 19, 17, 24, 56, 715, DateTimeKind.Local).AddTicks(3896),
                            CreatedBy = 1L,
                            IsDeleted = false,
                            Name = "Administrator",
                            OrganizationId = 0L,
                            Password = "e10adc3949ba59abbe56e057f20f883e"
                        });
                });

            modelBuilder.Entity("MicroService.Models.UserRole", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .UseIdentityColumn();

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("datetime2");

                    b.Property<long>("CreatedBy")
                        .HasColumnType("bigint");

                    b.Property<DateTime?>("DeletedAt")
                        .HasColumnType("datetime2");

                    b.Property<long?>("DeletedBy")
                        .HasColumnType("bigint");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("bit");

                    b.Property<DateTime?>("ModifiedAt")
                        .HasColumnType("datetime2");

                    b.Property<long?>("ModifiedBy")
                        .HasColumnType("bigint");

                    b.Property<long>("RoleId")
                        .HasColumnType("bigint");

                    b.Property<long>("UserId")
                        .HasColumnType("bigint");

                    b.HasKey("Id");

                    b.ToTable("User_Roles");
                });
#pragma warning restore 612, 618
        }
    }
}
