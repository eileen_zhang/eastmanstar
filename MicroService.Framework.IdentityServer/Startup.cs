﻿
using MicroService.BaseService;
using MicroService.Framework.IdentityServer.Models;
using MicroService.Framework.IdentityServer.Services;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Security.Cryptography.X509Certificates;

namespace MicroService.Framework.IdentityServer
{
    public class Startup : BaseStartup
    {
        IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public override void AfterConfigureServices(IServiceCollection services)
        {
            services.Configure<AuthModel>(_configuration.GetSection("Authorizations"));
            var identityServerSettings = _configuration.GetSection("IdentityServerSettings");
            services.AddIdentityServer()
                .AddSigningCredential(new X509Certificate2("Credential.pfx", identityServerSettings["CredentialPassword"]))
                .AddInMemoryApiResources(Config.GetApiResources(identityServerSettings))
                .AddInMemoryApiScopes(Config.GetApiScopes(identityServerSettings))
                .AddInMemoryClients(Config.GetClients(identityServerSettings))
                .AddResourceOwnerValidator<ResourceOwnerPasswordValidator>()
                .AddProfileService<ProfileService>();

            services.AddTransient<AuthService>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public override void AfterConfigure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseIdentityServer();
        }


    }
}
