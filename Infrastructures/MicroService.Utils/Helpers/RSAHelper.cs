﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace MicroService.Utils
{
    /// <summary>
    /// RSA encrypt and decrypt helper
    /// </summary>
    public class RSAHelper
    {
        /// <summary>
        /// Encrypts the specified data.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="pubkey">The pubkey.</param>
        /// <returns></returns>
        public static byte[] Encrypt(byte[] data, string pubkey)
        {
            using (var rsaProvider = new RSACryptoServiceProvider())
            {
                var inputBytes = data;
                rsaProvider.FromXmlString(pubkey);//load public key
                int bufferSize = (rsaProvider.KeySize / 8) - 11;
                var buffer = new byte[bufferSize];
                using (MemoryStream inputStream = new MemoryStream(inputBytes),
                     outputStream = new MemoryStream())
                {
                    while (true)
                    {
                        int readSize = inputStream.Read(buffer, 0, bufferSize);
                        if (readSize <= 0)
                        {
                            break;
                        }

                        var temp = new byte[readSize];
                        Array.Copy(buffer, 0, temp, 0, readSize);
                        var encryptedBytes = rsaProvider.Encrypt(temp, false);
                        outputStream.Write(encryptedBytes, 0, encryptedBytes.Length);
                    }
                    return outputStream.ToArray();
                }
            }
        }

        /// <summary>
        /// Encrypts the specified data.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="pubkey">The pubkey.</param>
        /// <returns></returns>
        public static string Encrypt(string data, string pubkey)
        {
            byte[] bs = Encoding.UTF8.GetBytes(data);
            bs = Encrypt(bs, pubkey);
            return Convert.ToBase64String(bs);
        }

        /// <summary>
        /// Decrypts the specified data.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="prikey">The prikey.</param>
        /// <returns></returns>
        public static byte[] Decrypt(byte[] data, string prikey)
        {
            using (var rsaProvider = new RSACryptoServiceProvider())
            {
                var inputBytes = data;
                rsaProvider.FromXmlString(prikey);
                int bufferSize = rsaProvider.KeySize / 8;
                var buffer = new byte[bufferSize];
                using (MemoryStream inputStream = new MemoryStream(inputBytes),
                     outputStream = new MemoryStream())
                {
                    while (true)
                    {
                        int readSize = inputStream.Read(buffer, 0, bufferSize);
                        if (readSize <= 0)
                        {
                            break;
                        }

                        var temp = new byte[readSize];
                        Array.Copy(buffer, 0, temp, 0, readSize);
                        var rawBytes = rsaProvider.Decrypt(temp, false);
                        outputStream.Write(rawBytes, 0, rawBytes.Length);
                    }
                    return outputStream.ToArray();
                }
            }
        }


        /// <summary>
        /// Decrypts the specified data.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="prikey">The prikey.</param>
        /// <returns></returns>
        public static string Decrypt(string data, string prikey)
        {
            byte[] bs = Convert.FromBase64String(data);
            bs = Decrypt(bs, prikey);
            return Encoding.UTF8.GetString(bs);
        }

        /// <summary>
        /// Signs the data.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="prikey">The prikey.</param>
        /// <returns></returns>
        public static byte[] SignData(byte[] data, string prikey)
        {
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            rsa.FromXmlString(prikey);
            return rsa.SignData(data, new SHA1CryptoServiceProvider());
        }

        /// <summary>
        /// Signs the data.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="prikey">The prikey.</param>
        /// <returns></returns>
        public static string SignData(string data, string prikey)
        {
            byte[] bs = Encoding.UTF8.GetBytes(data);
            byte[] recbs = SignData(bs, prikey);
            return Convert.ToBase64String(recbs);
        }


        /// <summary>
        /// Vers the sign data.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="signData">The sign data.</param>
        /// <param name="pubkey">The pubkey.</param>
        /// <returns></returns>
        public static bool VerSignData(byte[] data, byte[] signData, string pubkey)
        {
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            rsa.FromXmlString(pubkey);
            return rsa.VerifyData(data, new SHA1CryptoServiceProvider(), signData);
        }


        /// <summary>
        /// Vers the sign data.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="signData">The sign data.</param>
        /// <param name="pubkey">The pubkey.</param>
        /// <returns></returns>
        public static bool VerSignData(string data, string signData, string pubkey)
        {
            byte[] dbs = Encoding.UTF8.GetBytes(data);
            byte[] sbs = Convert.FromBase64String(signData);
            return VerSignData(dbs, sbs, pubkey);
        }


        /// <summary>
        /// Creates the key file.
        /// </summary>
        public static void CreateKeyFile()
        {
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            string prikey = rsa.ToXmlString(true);
            string pubkey = rsa.ToXmlString(false);
            System.IO.StreamWriter sw = new System.IO.StreamWriter(System.AppDomain.CurrentDomain.BaseDirectory + "keyinfo.txt", false);
            sw.WriteLine("pubkey:" + pubkey);
            sw.WriteLine("prikey:" + prikey);
            sw.Close();
        }
    }


    /// <summary>
    /// Des encrypt and decrypt tool
    /// </summary>
    public class DesTool
    {

        /// <summary>
        /// Decrypts the specified data.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        /// <exception cref="Exception">The ciphertext and key do not match!</exception>
        public static byte[] Decrypt(byte[] data, string key)
        {
            try
            {
                MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
                byte[] bsmy = md5.ComputeHash(Encoding.UTF8.GetBytes(key));
                TripleDESCryptoServiceProvider t = new TripleDESCryptoServiceProvider
                {
                    Key = bsmy,
                    Mode = CipherMode.ECB
                };
                byte[] bs = t.CreateDecryptor().TransformFinalBlock(data, 0, data.Length);
                return bs;
            }
            catch (Exception)
            {
                throw new Exception("The ciphertext and key do not match!");
            }
        }

        /// <summary>
        /// Decrypts the specified data.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public static string Decrypt(string data, string key)
        {
            byte[] bs = Convert.FromBase64String(data);
            byte[] recbs = Decrypt(bs, key);
            return Encoding.UTF8.GetString(recbs);
        }

        /// <summary>
        /// Encrypts the specified data.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public static byte[] Encrypt(byte[] data, string key)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] bsmy = md5.ComputeHash(Encoding.UTF8.GetBytes(key));
            TripleDESCryptoServiceProvider t = new TripleDESCryptoServiceProvider
            {
                Key = bsmy,
                Mode = CipherMode.ECB
            };
            byte[] bs = t.CreateEncryptor().TransformFinalBlock(data, 0, data.Length);
            return bs;
        }

        /// <summary>
        /// Encrypts the specified data.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public static string Encrypt(string data, string key)
        {
            byte[] bs = Encoding.UTF8.GetBytes(data);
            byte[] recbs = Encrypt(bs, key);
            return Convert.ToBase64String(recbs);
        }
    }
}
