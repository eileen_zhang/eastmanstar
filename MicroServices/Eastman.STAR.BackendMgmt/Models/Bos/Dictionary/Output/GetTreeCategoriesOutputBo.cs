﻿using System.Collections.Generic;

namespace Eastman.STAR.BackendMgmt.Models.Bos
{
    /// <summary>
    /// get tree categories output business model
    /// </summary>
    public class GetTreeCategoriesOutputBo
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public long Id { get; set; }
        /// <summary>
        /// Gets or sets the name of the category.
        /// </summary>
        /// <value>
        /// The name of the category.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the category code.
        /// </summary>
        /// <value>
        /// The category code.
        /// </value>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the sub dictionaries.
        /// </summary>
        /// <value>
        /// The sub dictionaries.
        /// </value>
        public List<GetTreeDictionariesOutputBo> Dictionaries { get; set; }
    }
}
