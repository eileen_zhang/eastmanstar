﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace MicroService.Utils.Extensions
{
    public static class RegexExtension
    {
        public static bool CheckNumberOfContainsNumberAndLetter(this string input) => Regex.Match(input, @"^[A-Za-z0-9]+$").Success;
    }
}
