﻿using MicroService.Models;
using System.Collections.Generic;

namespace Eastman.STAR.BackendMgmt.Models.Dtos
{
    /// <summary>
    /// UserPage  Output
    /// </summary>
    public class UserPageOutput
    {
        /// <summary>
        /// Gets or sets the total.
        /// </summary>
        /// <value>
        /// The total.
        /// </value>
        public int Total { get; set; }

        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        /// <value>
        /// The data.
        /// </value>
        public List<User> Data { get; set; } = new List<User>();
    }
}
