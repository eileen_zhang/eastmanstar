﻿using AutoMapper;
using Eastman.STAR.BackendMgmt.Services;
using MicroService.BaseService;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Eastman.STAR.BackendMgmt
{
    /// <summary>
    /// Startup
    /// </summary>
    /// <seealso cref="BaseStartup" />
    public class Startup : BaseStartup
    {
        IConfiguration _configuration;

        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/> class.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        /// <summary>
        /// do some action, like use some middleware etc.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public override void AfterConfigure(IApplicationBuilder app, IWebHostEnvironment env)
        {

        }

        /// <summary>
        /// do some action, like injection etc.
        /// </summary>
        /// <param name="services"></param>
        public override void AfterConfigureServices(IServiceCollection services)
        {
            //add bearer auth
            services.AddAuthentication("Bearer").AddJwtBearer("Bearer", options =>
            {
                var section = _configuration.GetSection("IdentityServerSettings");
                options.Authority = section["Authority"];//IdentityServer address
                options.RequireHttpsMetadata = false;
                options.Audience = section["Audience"];
            });

            //inject business object
            services.AddTransient<UserServices>();
            services.AddTransient<RoleServices>();
            services.AddTransient<PermissionServices>();
            services.AddTransient<DictionaryServices>();
            services.AddTransient<CommonServices>();

            //add automapper
            services.AddAutoMapper(typeof(AutoMapperProfile));
        }
    }
}
