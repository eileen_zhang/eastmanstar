﻿using MicroService.BaseService.Context.Configurations;
using MicroService.Models;
using MicroService.Models.Entities.BaseManage;
using Microsoft.EntityFrameworkCore;

namespace MicroService.BaseService
{
    /// <summary>
    /// DbContext
    /// </summary>
    public class STARContext : DbContext
    {
        public STARContext(DbContextOptions<STARContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //initialize datas and entity
            EntityCfg.EntitySettings(modelBuilder);
            EntityCfg.InitializeDatas(modelBuilder);

            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        public DbSet<User> Users { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Permission> Permissions { get; set; }

        public DbSet<UserRole> User_Roles { get; set; }

        public DbSet<EmaiAuthToken> EmaiAuthToken { get; set; }

        public DbSet<ConfigDictionary> ConfigDictionarys { get; set; }
    }
}
