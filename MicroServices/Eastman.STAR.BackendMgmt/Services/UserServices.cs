﻿using AutoMapper;
using Eastman.STAR.BackendMgmt.Models.Dtos;
using MicroService.Models;
using MicroService.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eastman.STAR.BackendMgmt.Services
{
    /// <summary>
    /// User Services
    /// </summary>
    public class UserServices
    {
        IRepository<User> _repositoryUser;
        IRepository<UserRole> _repositoryUserRole;
        IMapper _mapper;
        IUnitOfWork _unitOfWork;
        /// <summary>
        /// Initializes a new instance of the <see cref="UserServices"/> class.
        /// </summary>
        /// <param name="unitOfWork">The unit of work.</param>
        /// <param name="mapper">The mapper.</param>
        public UserServices(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _repositoryUser = unitOfWork.GetStore<User>();
            _repositoryUserRole = unitOfWork.GetStore<UserRole>();
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Get a user by account
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        public Task<User> GetUser(string account)
        {
            return _repositoryUser.Entities.SingleOrDefaultAsync(a => a.Account == account);
        }

        /// <summary>
        /// Get a user by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<UserOutput> GetUserInfo(long id)
        {
            var userOutput = _mapper.Map<UserOutput>(_repositoryUser.Entities.SingleOrDefaultAsync(a => a.Id == id).Result);

            userOutput.Roles = _repositoryUserRole.Entities.Where(a => a.UserId == id).Select(b => b.RoleId).ToArray();

            return Task.FromResult(userOutput);
        }

        /// <summary>
        /// Gets the user information.
        /// </summary>
        /// <param name="account">The account.</param>
        /// <returns></returns>
        public Task<UserOutput> GetUserInfo(string account)
        {
            var userOutput = _mapper.Map<UserOutput>(_repositoryUser.Entities.SingleOrDefaultAsync(a => a.Account == account).Result);

            userOutput.Roles = _repositoryUserRole.Entities.Where(a => a.UserId == userOutput.Id).Select(b => b.RoleId).ToArray();

            return Task.FromResult(userOutput);
        }

        /// <summary>
        /// Get user by id and account
        /// </summary>
        /// <param name="id"></param>
        /// <param name="account"></param>
        /// <returns></returns>
        public Task<User> CheckUser(long id, string account)
        {
            return _repositoryUser.Entities.SingleOrDefaultAsync(a => a.Id == id && a.Account == account);
        }

        /// <summary>
        /// Get users
        /// </summary>
        /// <param name="name"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public Task<UserPageOutput> GetUsers(string name, int pageIndex, int pageSize)
        {
            return Task.FromResult(new UserPageOutput
            {
                Total = _repositoryUser.Entities.Where(a => string.IsNullOrEmpty(name) || a.Name.Contains(name)).Count(),
                Data = _repositoryUser.Entities.Where(a => string.IsNullOrEmpty(name) || a.Name.Contains(name)).Skip(pageIndex * pageSize).Take(pageSize).ToList()
            });
        }

        /// <summary>
        /// Saves the user.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="roles">The roles.</param>
        /// <returns></returns>
        public Task<int> SaveUser(User user, List<long> roles)
        {

            if (user.Id > 0)
            {
                _repositoryUser.Modify(user);

                var UserRoles = _repositoryUserRole.TrackEntities.Where(a => a.UserId == user.Id).ToList();

                foreach (var UserRole in UserRoles)
                {
                    _repositoryUserRole.Delete(UserRole);
                }
            }
            else
            {
                _repositoryUser.Add(user);
            }
            _unitOfWork.BeginTransaction();


            foreach (var roleId in roles)
            {
                _repositoryUserRole.Add(new UserRole
                {
                    UserId = user.Id,
                    RoleId = roleId,
                });
            }


            var rows = _repositoryUser.SaveChangesAsync();

            _repositoryUserRole.SaveChangesAsync();

            _unitOfWork.Commit();

            return rows;
        }

        /// <summary>
        /// Logic delete user
        /// </summary>
        /// <param name="id"></param>
        /// <param name="operator"></param>
        /// <returns></returns>
        public async Task<int> DeleteUser(long id, User @operator)
        {
            var user = await _repositoryUser.Entities.Where(a => a.Id == id).SingleOrDefaultAsync();
            user.IsDeleted = true;
            user.DeletedAt = DateTime.Now;
            user.DeletedBy = @operator.Id;

            _repositoryUser.Modify(user);
            return await _repositoryUser.SaveChangesAsync();
        }
    }
}
