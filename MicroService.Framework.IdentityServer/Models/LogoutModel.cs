﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.Framework.IdentityServer.Models.Dtos
{
    /// <summary>
    /// LogoutInputModel
    /// </summary>
    public class LogoutModel
    {
        /// <summary>
        /// token
        /// </summary>
        public string Token { get; set; }
    }
}
