﻿using MicroService.Models.Enums;
using System.ComponentModel.DataAnnotations;

namespace MicroService.Models
{
    /// <summary>
    /// Dictionary Entity
    /// </summary>
    /// <seealso cref="BaseEntity{Int64}" />
    public class ConfigDictionary : BaseEntity<long>
    {
        /// <summary>
        /// Gets or sets the dictionary status.
        /// </summary>
        /// <value>
        /// The dictionary status.
        /// </value>
        public DictType DictionaryType { get; set; }

        /// <summary>
        /// Gets or sets the category code.
        /// </summary>
        /// <value>
        /// The category code.
        /// </value>
        [StringLength(100)]
        public string ParentCode { get; set; }

        /// <summary>
        /// Gets or sets the name of the type.
        /// </summary>
        /// <value>
        /// The name of the type.
        /// </value>
        [StringLength(50)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        [StringLength(100)]
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the order.
        /// </summary>
        /// <value>
        /// The order.
        /// </value>
        public int Order { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [StringLength(500)]
        public string Description { get; set; }
    }
}
