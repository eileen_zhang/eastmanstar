﻿using IdentityModel.Client;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Newtonsoft.Json;

using System;
using System.Collections.Specialized;
using System.Net;
using System.Net.Http;
using System.Text;

namespace UnitTestProject1
{
    [TestClass]
    public class IdentityServerTest
    {
        const string _identityServerAddr = "https://localhost:10086";
        //const string _identityServerAddr = "http://localhost:10000/IdentityServer";

        [TestMethod]
        public void GetToken()
        {
            var datas = new NameValueCollection();
            //datas.Add("grant_type", "client_credentials");
            datas.Add("grant_type", "password");
            datas.Add("username", "admin");
            datas.Add("password", "e10adc3949ba59abbe56e057f20f883e");
            datas.Add("client_id", "BackendClient");
            datas.Add("client_secret", "dcdcfd30bd954f5a846a74d40f70bbe3");
            datas.Add("scope", "BackendApi");

            using (var client = new WebClient())
            {
                var buffer = client.UploadValues($"{_identityServerAddr}/connect/token", datas);
                var result = Encoding.UTF8.GetString(buffer);

                var data = JsonConvert.DeserializeObject<dynamic>(result);
                client.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + data.access_token.ToString());

                //result = client.DownloadString("http://localhost:10000/Management/api/Home/Haha");
                result = client.DownloadString("http://localhost:10000/TestService1/api/Home/Haha");
                //result = Encoding.UTF8.GetString(buffer);
                Console.WriteLine(result);
            }
        }

        [TestMethod]
        public void GetToken1()
        {
            var httpClient = new HttpClient();
            var disco = httpClient.GetDiscoveryDocumentAsync(new DiscoveryDocumentRequest
            {
                Address = _identityServerAddr,
                Policy =
                {
                     RequireHttps=false,
                     ValidateIssuerName=false,
                     ValidateEndpoints=false
                }
            }).Result;
            if (disco.IsError)
            {
                throw new Exception(disco.Error);
            }
            var tokenResponse = httpClient.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
            {
                Address = disco.TokenEndpoint,
                ClientId = "BackendClient",
                ClientSecret = "dcdcfd30bd954f5a846a74d40f70bbe3",
                Scope = "BackendApi"
            });
            string token = tokenResponse.Result.AccessToken;
            Console.Write(token);
        }

        [TestMethod]
        public void Test1()
        {
            var httpClient = new HttpClient();
            var disco = httpClient.GetDiscoveryDocumentAsync(new DiscoveryDocumentRequest
            {
                Address = _identityServerAddr,
                Policy =
                {
                     RequireHttps=false,
                }
            }).Result;
            if (disco.IsError)
            {
                throw new Exception(disco.Error);
            }
            var tokenResponse = httpClient.RequestPasswordTokenAsync(new PasswordTokenRequest
            {
                Address = disco.TokenEndpoint,
                UserName = "admin",
                Password = "123456789",
                ClientId = "client",
                ClientSecret = "dcdcfd30bd954f5a846a74d40f70bbe3",
                Scope = "api1"
            });
            string token = tokenResponse.Result.AccessToken;
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
            var result = httpClient.GetStringAsync("http://localhost:10000/TestService1/api/Home/Haha").Result;
            Console.WriteLine(result);
            //}
        }

        [TestMethod]
        public void TestLogin()
        {
            using (var client = new HttpClient())
            {
                //var result = client.GetStringAsync($"{_identityServerAddr}/api/Auth/BackendAuth?account=admin&pwd=e10adc3949ba59abbe56e057f20f883e").Result;

                //var json = JsonConvert.DeserializeObject<dynamic>(result);

                //string token = json.data;
                string token = "eyJhbGciOiJSUzI1NiIsImtpZCI6IjY2NjY2N0EyMTVEOEZGNTlFMDU2N0Q3MTk3QjA3QjY3Qjc5MzYzQkVSUzI1NiIsInR5cCI6ImF0K2p3dCIsIng1dCI6IlptWm5vaFhZXzFuZ1ZuMXhsN0I3WjdlVFk3NCJ9.eyJuYmYiOjE2MTQ0OTYyMDksImV4cCI6MTYxNDQ5OTgwOSwiaXNzIjoiaHR0cDovL3BjLXBlbmdnYW5nOjEwMDg2IiwiYXVkIjoiQmFja2VuZEFwaSIsImNsaWVudF9pZCI6IkJhY2tlbmRDbGllbnQiLCJzdWIiOiJhZG1pbiIsImF1dGhfdGltZSI6MTYxNDQ5NjIwOSwiaWRwIjoibG9jYWwiLCJBY2NvdW50IjoiYWRtaW4iLCJqdGkiOiJFMjU4NUNGMjhDQjgxMzc5QTUxMkUzMEYyQjczMUFEMCIsImlhdCI6MTYxNDQ5NjIwOSwic2NvcGUiOlsiQmFja2VuZEFwaSJdLCJhbXIiOlsicHdkIl19.cpSMWoG9SzZ56m1tNtJHS2qeg0Zpe6mImRG5mdoAPTjWsWn1aF9e1h5NKBY0Zkkn19pKuJ3hgLrKPyyJSkQiKHfE3Ro7nkmT5w5k0zTz-_TcHu9tAczjmuHuxvlBZ7bDken9pJtGDYZOUikv1nWo2TC5e9jlxiZW__P6H_BwtCQCRBUDoz0dLWhJZsuvjLGgmKm7d3gseKqLJEdDjgiYxLQ6EczyJJbggGtYMinkxJacnfXmX1sQG19Cu14GN9XKjpE_pXx0-Q24_q9mW-qHtw0iobYqGe9bxCkv6ElWAjnai2m4kWwzQBdszuvqfo2F00IaTMGR39EGrbuSzuDU2Q";
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
                var result = client.GetStringAsync("http://localhost:10000/TestService1/api/Home/Haha").Result;
            }

        }

    }
}
