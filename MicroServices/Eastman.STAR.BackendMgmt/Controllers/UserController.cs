﻿using AutoMapper;
using Eastman.STAR.BackendMgmt.Models.Dtos;
using Eastman.STAR.BackendMgmt.Services;
using MicroService.BaseService.Controllers;
using MicroService.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Eastman.STAR.BackendMgmt.Controllers
{
    /// <summary>
    /// The user related
    /// </summary>
    /// <response code="90000">Operate successfully</response>
    /// <response code="90999">Operate failed</response>
    /// <response code="99999">System exception</response>
    [ProducesResponseType(90000),
        ProducesResponseType(90999),
        ProducesResponseType(99999)]
    public class UserController : BaseAPIController
    {
        UserServices _userService;
        IMapper _mapper;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="userService"></param>
        /// <param name="mapper"></param>
        public UserController(UserServices userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }

        /// <summary>
        /// Get current user
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(UserOutput), 90000)]
        public async Task<IActionResult> GetCurrentUser()
        {
            try
            {
                var claim = HttpContext.User.Claims.FirstOrDefault(a => a.Type == "Account");
                if (claim == null)
                {
                    return Ok(MessageResult.FailureResult(MessageModel.NoAuthenticationMessage));
                }

                var user = await _userService.GetUserInfo(claim.Value);
                return user == null
                    ? Ok(MessageResult.FailureResult())
                    : Ok(MessageResult.SuccessResult(new UserOutput { Account = user.Account, Name = user.Name, Avatar = user.Avatar, Introduction = user.Avatar, Roles = user.Roles }));
            }
            catch (Exception ex)
            {
                return Ok(MessageResult.FailureResult(new MessageModel(9999, ex.Message)));
            }
        }

        /// <summary>
        /// Get users
        /// </summary>
        /// <param name="name"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(UserPageOutput), 90000)]
        public async Task<IActionResult> GetUsers(string name, int pageIndex, int pageSize)
        {
            try
            {
                var users = await _userService.GetUsers(name, pageIndex, pageSize);
                return Ok(MessageResult.SuccessResult(users));
            }
            catch (Exception ex)
            {
                return Ok(MessageResult.FailureResult(new MessageModel(9999, ex.Message)));
            }
        }

        /// <summary>
        /// Get user by Id
        /// </summary>
        /// <param name="userId">user id</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(UserOutput), 90000)]
        public async Task<IActionResult> GetUserById(long userId)
        {
            try
            {
                var users = await _userService.GetUserInfo(userId);
                return Ok(MessageResult.SuccessResult(users));
            }
            catch (Exception ex)
            {
                return Ok(MessageResult.FailureResult(new MessageModel(9999, ex.Message)));
            }
        }

        /// <summary>
        /// Save user
        /// </summary>
        /// <param name="userInput"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(MessageModel), 90000)]
        public async Task<IActionResult> SaveUser(UserInput userInput)
        {
            try
            {
                User user;
                //modify
                if (userInput.Id > 0)
                {
                    user = await _userService.CheckUser(userInput.Id.Value, userInput.Account);
                    if (user == null)
                    {
                        return Ok(MessageResult.FailureResult(MessageModel.IdOrAccountChangedMessage));
                    }

                    _mapper.Map(userInput, user);
                    user.ModifiedAt = DateTime.Now;
                    user.ModifiedBy = CurrentUser.Id;
                }
                else
                {
                    user = await _userService.GetUser(userInput.Account);
                    if (user != null)
                    {
                        return Ok(MessageResult.FailureResult(MessageModel.AccountAlreadyExistedMessage));
                    }

                    user = _mapper.Map<User>((userInput));
                    user.CreatedAt = DateTime.Now;
                    user.CreatedBy = CurrentUser.Id;
                }

                var result = await _userService.SaveUser(user, userInput.Roles);
                return result > 0 ? Ok(MessageResult.SuccessResult()) : Ok(MessageResult.FailureResult());
            }
            catch (Exception ex)
            {
                return Ok(MessageResult.FailureResult(new MessageModel(9999, ex.Message)));
            }
        }

        /// <summary>
        /// Deletes the user.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(MessageResult), 90000)]
        public async Task<IActionResult> DeleteUser([FromBody] UserIdInput request)
        {
            try
            {
                var result = await _userService.DeleteUser(request.UserId, CurrentUser);
                return result == 1 ? Ok(MessageResult.SuccessResult()) : Ok(MessageResult.FailureResult());
            }
            catch (Exception ex)
            {
                return Ok(MessageResult.FailureResult(new MessageModel(9999, ex.Message)));
            }
        }
    }
}
