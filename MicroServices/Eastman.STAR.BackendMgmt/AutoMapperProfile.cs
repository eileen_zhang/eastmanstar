﻿using AutoMapper;
using Eastman.STAR.BackendMgmt.Models.Bos;
using Eastman.STAR.BackendMgmt.Models.Dtos;
using MicroService.Models;

namespace Eastman.STAR.BackendMgmt
{
    /// <summary>
    /// AutoMapper settings class
    /// </summary>
    public class AutoMapperProfile : Profile
    {
        /// <summary>
        /// AutoMapper Profile
        /// </summary>
        public AutoMapperProfile()
        {
            CreateMap<User, UserInput>().ReverseMap();
            CreateMap<User, UserOutput>().ReverseMap();

            CreateMap<GetRolesInput, GetRolesBo>().ReverseMap();
            CreateMap<SaveRoleInput, SaveRoleBo>().ReverseMap();
            CreateMap<SaveRoleBo, Role>().ReverseMap();
            CreateMap<RoleOutputBo, Role>().ReverseMap();
            CreateMap<RoleOutput, RoleOutputBo>().ReverseMap();

            CreateMap<GetTreeCategoriesOutputBo, ConfigDictionary>().ReverseMap();
            CreateMap<GetTreeDictionariesOutputBo, ConfigDictionary>().ReverseMap();
            CreateMap<GetTreeSubDictionariesOutputBo, ConfigDictionary>().ReverseMap();
            CreateMap<AddDictionaryValueOutputBo, ConfigDictionary>().ReverseMap();
            CreateMap<GetTreeCategoriesOutput, GetTreeCategoriesOutputBo>().ReverseMap();
            CreateMap<GetTreeDictionariesOutput, GetTreeDictionariesOutputBo>().ReverseMap();
            CreateMap<GetTreeSubDictionariesOutput, GetTreeSubDictionariesOutputBo>().ReverseMap();
            CreateMap<AddDictionaryValueOutput, AddDictionaryValueOutputBo>().ReverseMap();

            CreateMap<UpdateDictionaryBo, ConfigDictionary>().ReverseMap();
            CreateMap<UpdateDictionaryBo, UpdateDictionaryInput>().ReverseMap();
            CreateMap<SaveDictionaryValueBo, SaveDictionaryValueInput>().ReverseMap();
            CreateMap<SaveDictionaryValueBo, ConfigDictionary>().ReverseMap();
            CreateMap<CodeMessageOutput, CodeMessageBo>().ReverseMap();
        }
    }
}
