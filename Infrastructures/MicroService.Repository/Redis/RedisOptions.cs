﻿namespace MicroService.Repository
{
    /// <summary>
    /// Redis Options
    /// </summary>
    public class RedisOptions
    {
        /// <summary>
        /// Gets or sets the connection string.
        /// </summary>
        /// <value>
        /// The connection string.
        /// </value>
        public string ConnectionString { get; set; }
    }
}
