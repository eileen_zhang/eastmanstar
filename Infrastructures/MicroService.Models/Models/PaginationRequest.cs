﻿namespace MicroService.Models
{
    /// <summary>
    /// Pagination Entity
    /// </summary>
    public class PaginationRequest
    {
        /// <summary>
        /// Current page
        /// </summary>
        public int PageIndex { get; set; }

        /// <summary>
        /// Number of items per page
        /// </summary>
        public int PageSize { get; set; } = 20;

        /// <summary>
        /// Total Count
        /// </summary>
        public int TotalCount { get; set; }
    }
}
