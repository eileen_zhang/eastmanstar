﻿namespace Eastman.STAR.BackendMgmt.Models.Bos
{
    /// <summary>
    /// code message business model
    /// </summary>
    public class CodeMessageBo
    {
        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>
        /// The code.
        /// </value>
        public int Code { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>
        /// The message.
        /// </value>
        public string Message { get; set; }
    }
}
