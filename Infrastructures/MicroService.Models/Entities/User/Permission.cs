﻿using System.ComponentModel.DataAnnotations;

namespace MicroService.Models
{
    /// <summary>
    /// Permission Entity
    /// </summary>
    /// <seealso cref="BaseEntity{Int64}" />
    public class Permission : BaseEntity<long>
    {
        /// <summary>
        /// ShortName
        /// </summary> 
        [MaxLength(50)]
        public string ResourceId { get; set; }

        /// <summary>
        /// Type
        /// </summary>
        [MaxLength(20)]
        public string Type { get; set; }

        /// <value>
        /// The role identifier.
        /// </value>
        public long RoleId { get; set; }

    }
}
