﻿using MicroService.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text.RegularExpressions;

namespace UnitTestProject1
{
    /// <summary>
    /// RSA test
    /// </summary>
    [TestClass]
    public class RSATest
    {
        /// <summary>
        /// Creates the RSA key.
        /// </summary>
        [TestMethod]
        public void CreateRSAKey()
        {
            RSAHelper.CreateKeyFile();
        }

        /// <summary>
        /// Decrypts the test.
        /// </summary>
        [TestMethod]
        public void DecryptTest()
        {
            string priKey = @"<RSAKeyValue><Modulus>vxhFG6vaazFhIjOEkuX7zjRV1Jm2AFalyhtQ9wxmJ3hnafx+J8WZSaIdvAL4FvRboRRdHEpCs5IebwSOjeS4w1Pa9TJsw358ozb1lhChdsFIYn7b7I+nL8qzVxumpLRBibf4tCrNl70JToTc7gOcRV/56aPIC/iX2SQJjHwa32k=</Modulus><Exponent>AQAB</Exponent><P>8kP0+wQ92pB/64VjNufcvUKBJ1C87vTQbstEB6FmgGjL35gVHVIIt25IkJF8LNmHxy4W8wOfV13MBQ72ouHgFw==</P><Q>ye2p9y9iqnxVDFjqVuVnXFmxfmzgIPP6NlIN5BBSE++pPeKaL/Ee86i02z+dUS9o3ODosz4M5WnaWtmzb3Vsfw==</Q><DP>yZ8A7T9Ucue7bjlunq1Mqj4E2sF4263nIa4NCgSrF8cflRAu0l4JXw9rWNWbcIABXiTfeJPVMpKfTr5rSJePNQ==</DP><DQ>vRVHaqd8hJgYK4+3H9eAigGTKwdYeIL9cZA0YbLGqNRqbgCa0PnAOfloIUXYUgVK9EnGnNEl0kzkot3oIB6ckQ==</DQ><InverseQ>jeAS3nrsRP5fzO6IYQUsKW8kDc5gOz1fuAnzguSD6LPxGIaJhEA/4rV6DzU9OHqRiGi0PnSv4Ljib7hkXheQDA==</InverseQ><D>bg7bVomOvWfuKpWDSFuev+9Sc6SntuAwkgtTgUWp3Bezb85VFjDvDQKKn4sSOZ4427fvvGhkrCl+z6/PLpfJdO4VQDCy95qzsxSRI1j+P8JkIBarJ1xKWtTf51Lse8YRJqWn6MNzhQJBDkYfkyHvbcDhFizG5II18dcVnXEOi5U=</D></RSAKeyValue>";

            var result = RSAHelper.Decrypt(@"ZD+MFnvDWn/fJ4wXlNDJbrv7GnIv7x7HwWdcFNgY/QOqaDzUUGaKAAbf7R9VKc81+UboA6+Q3LbVD5D1LR8yBPLw9LhTTvmMG78isFD7WYiP3xt68wHxZzbPLBApVt72Jz8jdoYbj7Pyh3D2T1MaEmWjr1kBAoPWFEsLE6uQh+k=", priKey);
        }

        [TestMethod]
        public void RegexMatchTest()
        {
            string param = "{\"Params\":\"123456789ssssssssssssssssss\"}";

            var match = Regex.Match(param, "(<?param>(123456789ssssssssssssssssss))");
        }
    }
}
