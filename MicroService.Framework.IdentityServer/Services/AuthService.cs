﻿using IdentityModel.Client;

using MicroService.Framework.IdentityServer.Models;
using MicroService.Models;
using MicroService.Models.Entities.BaseManage;
using MicroService.Repository;
using MicroService.Utils.Email;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

using System;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MicroService.Framework.IdentityServer.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class AuthService
    {
        AuthModel _authModel;
        IRepository<User> _repositoryUser;
        IRepository<EmaiAuthToken> _repositoryEmaiAuthToken;
        IHttpContextAccessor _httpContextAccessor;
        IConfiguration _configuration;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthService"/> class.
        /// </summary>
        /// <param name="options">The options.</param>
        /// <param name="repositoryUser">The repository user.</param>
        /// <param name="repositoryEmaiAuthToken">The repository emai authentication token.</param>
        /// <param name="httpContextAccessor">The HTTP context accessor.</param>
        /// <param name="configuration">The configuration.</param>
        public AuthService(IOptions<AuthModel> options, IRepository<User> repositoryUser, IRepository<EmaiAuthToken> repositoryEmaiAuthToken, IHttpContextAccessor httpContextAccessor, IConfiguration configuration)
        {
            _authModel = options.Value;
            _repositoryUser = repositoryUser;
            _repositoryEmaiAuthToken = repositoryEmaiAuthToken;
            _httpContextAccessor = httpContextAccessor;
            _configuration = configuration;
        }

        /// <summary>
        /// Generate token
        /// </summary>
        /// <param name="account"></param>
        /// <param name="pwd"></param>
        /// <returns></returns>
        public async Task<string> BackendAuth(string account, string pwd)
        {
            var httpClient = new HttpClient();
            var disco = await httpClient.GetDiscoveryDocumentAsync(new DiscoveryDocumentRequest
            {
                Address = $"{_httpContextAccessor.HttpContext.Request.Scheme}://{_httpContextAccessor.HttpContext.Request.Host.Value}",
                Policy =
                {
                     RequireHttps = false,
                     ValidateEndpoints = false,
                     ValidateIssuerName = false
                }
            });
            if (disco.IsError)
            {
                throw new Exception(disco.Error);
            }
            var tokenResponse = await httpClient.RequestPasswordTokenAsync(new PasswordTokenRequest
            {
                Address = disco.TokenEndpoint,
                UserName = account,
                Password = pwd,
                ClientId = _authModel.BackendAuth.ClientId,
                ClientSecret = _authModel.BackendAuth.ClientSecret,
                Scope = _authModel.BackendAuth.Scope
            });
            return tokenResponse.AccessToken;
        }

        /// <summary>
        /// Logouts the specified token.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns></returns>
        public async Task<TokenRevocationResponse> Logout(string token)
        {
            var httpClient = new HttpClient(); ;
            var disco = httpClient.GetDiscoveryDocumentAsync(new DiscoveryDocumentRequest
            {
                Address = $"{_httpContextAccessor.HttpContext.Request.Scheme}://{_httpContextAccessor.HttpContext.Request.Host.Value}",
                Policy =
                {
                     RequireHttps = false,
                     ValidateEndpoints = false,
                     ValidateIssuerName = false
                }
            }).Result;

            var result = await httpClient.RevokeTokenAsync(new TokenRevocationRequest
            {
                Address = disco.RevocationEndpoint,
                ClientId = _authModel.BackendAuth.ClientId,
                ClientSecret = _authModel.BackendAuth.ClientSecret,
                Token = token
            });

            return result;
        }

        public string MiniProgramAuth()
        {
            return string.Empty;
        }

        /// <summary>
        /// Get user by account and pwd.
        /// </summary>
        /// <param name="account"></param>
        /// <param name="pwd"></param>
        /// <returns></returns>
        public Task<User> GetUser(string account, string pwd)
        {
            return _repositoryUser.Entities.FirstOrDefaultAsync(a => a.Account == account && a.Password == pwd);
        }


        /// <summary>
        /// SendResetPasswordEmail
        /// </summary>
        /// <param name="authEmail"></param>
        /// <returns></returns>
        public Task<int> SendResetPasswordEmail(string authEmail)
        {
            User user = _repositoryUser.Entities.Where(a => a.Email == authEmail && !a.IsDeleted).FirstOrDefaultAsync().Result;

            if (user == null)
            {
                return Task.FromResult(30000);
            }
            string authToken = Guid.NewGuid().ToString();
            _repositoryEmaiAuthToken.Add(new EmaiAuthToken
            {
                AuthToken = authToken,
                AuthEmail = authEmail,
                CreatedAt = DateTime.Now
            });


            if (_repositoryEmaiAuthToken.SaveChanges() > 0)
            {
                var authEmailConfiguration = _configuration.GetSection("Email").GetSection("AuthEmail");
                string serviceUrl = authEmailConfiguration["AuthUrl"];
                StringBuilder content = new StringBuilder();
                content.Append("<div class=\"container\">");
                content.Append("  <div class=\"logo\"></div>");
                content.Append("  <div class=\"panel\">");
                content.Append("    <div class=\"panel-header\">密码重置</div>");
                content.Append("    <div class=\"panel-body\">");
                content.Append("      <p>");
                content.Append("        您好 " + authEmail);
                content.Append("      </p>");
                content.Append("      <p>您已经请求了重置密码，可以点击下面的链接来重置密码。</p>");
                content.Append("      <p>");
                content.Append("        <a");
                content.Append("          href=\"" + serviceUrl + "reset-password?AuthEmail=" + authEmail + "&AuthEmailToken=" + authToken);
                content.Append("          rel=\"noopener\"");
                content.Append("          target=\"_blank\"");
                content.Append("          >" + serviceUrl + "reset-password?AuthEmail=" + authEmail + "&AuthEmailToken=" + authToken);
                content.Append("      </p>");
                content.Append("      <p>如果您没有请求重置密码，请忽略这封邮件。</p>");
                content.Append("      <p>如果点链接无效,请复制到地址栏打开。</p>");
                content.Append("    </div>");
                content.Append("  </div>");
                content.Append("  <div class=\"footer\"></div>");
                content.Append("</div>");

                EmailSender emailSender = new EmailSender();

                emailSender.Send(new Mail
                {
                    From = authEmailConfiguration["AuthEmailFrom"],
                    FromDisplay = authEmailConfiguration["AuthEmailFromDisplay"],
                    MailBody = content.ToString(),
                    IsbodyHtml = true,
                    MailTitle = authEmailConfiguration["AuthEmailMailTitle"],
                    RecipientArry = new string[] {
                        authEmail
                    }
                });
                return Task.FromResult(10000);
            }
            else
            {
                return Task.FromResult(20000);
            }

        }


        /// <summary>
        /// Verifies the email token.
        /// </summary>
        /// <param name="authEmail">The authentication email.</param>
        /// <param name="authEmailToken">The authentication email token.</param>
        /// <returns></returns>
        public Task<int> VerifyEmailToken(string authEmail, string authEmailToken)
        {
            EmaiAuthToken emaiAuthToken = _repositoryEmaiAuthToken.Entities.Where(a => a.AuthEmail == authEmail && a.AuthToken == authEmailToken && !a.IsDeleted).OrderByDescending(a => a.CreatedAt).FirstOrDefaultAsync().Result;

            if (emaiAuthToken != null)
            {
                if ((emaiAuthToken.CreatedAt - DateTime.Now).TotalSeconds > 900)
                {
                    return Task.FromResult(-1);
                }
                return Task.FromResult(1);
            }
            else
            {
                return Task.FromResult(0);
            }

        }


        /// <summary>
        /// Updates the password by email token.
        /// </summary>
        /// <param name="authEmail">The authentication email.</param>
        /// <param name="authEmailToken">The authentication email token.</param>
        /// <param name="resetPassword">The reset password.</param>
        /// <returns></returns>
        public Task<int> UpdatePasswordByEmailToken(string authEmail, string authEmailToken, string resetPassword)
        {
            EmaiAuthToken emaiAuthToken = _repositoryEmaiAuthToken.Entities.Where(a => a.AuthEmail == authEmail && a.AuthToken == authEmailToken && a.IsDeleted != true).OrderByDescending(a => a.CreatedAt).FirstOrDefaultAsync().Result;

            if (emaiAuthToken != null)
            {
                if ((emaiAuthToken.CreatedAt - DateTime.Now).TotalSeconds > 900)
                {
                    return Task.FromResult(-1);
                }

                User user = _repositoryUser.Entities.Where(a => a.Email == authEmail).FirstOrDefaultAsync().Result;

                user.Password = resetPassword;

                _repositoryUser.Modify(user);

                _repositoryUser.SaveChanges();

                _repositoryEmaiAuthToken.Delete(emaiAuthToken);

                if (_repositoryEmaiAuthToken.SaveChanges() > 0)
                {
                    return Task.FromResult(1);
                }
                return Task.FromResult(2);
            }
            else
            {
                return Task.FromResult(0);
            }

        }

        /// <summary>
        /// Verifies the email token.
        /// </summary>
        /// <param name="authEmail">The authentication email.</param>
        /// <param name="authEmailToken">The authentication email token.</param>
        /// <returns></returns>
        public Task<int> VerifyEmail(string authEmail)
        {
            User user = _repositoryUser.Entities.Where(a => a.Email == authEmail && !a.IsDeleted).FirstOrDefaultAsync().Result;

            if (user != null)
            {
                return Task.FromResult(1);
            }
            else
            {
                return Task.FromResult(0);
            }

        }


    }
}
