﻿namespace MicroService.Models
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class MessageResult<T> : MessageResult
    {
        /// <summary>
        /// the return data
        /// </summary>
        public new T Data { get; set; }

        /// <summary>
        /// return successfully result
        /// </summary>
        /// <param name="data"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public static MessageResult<T> SuccessResult(T data = default, int total = 0)
        {
            return new MessageResult<T>
            {
                Success = true,
                Data = data,
                TotalCount = total,
                Message = MessageModel.GeneralSuccessfulMessage.Message,
                Code = MessageModel.GeneralSuccessfulMessage.Code
            };
        }

        /// <summary>
        /// return successfully result
        /// </summary>
        /// <param name="data"></param>
        /// <param name="messageModel"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public static MessageResult<T> SuccessResult(T data, MessageModel messageModel, int total = 0)
        {
            return new MessageResult<T>
            {
                Success = true,
                Data = data,
                TotalCount = total,
                Message = messageModel.Message,
                Code = messageModel.Code
            };
        }
    }

    /// <summary>
    /// The message result
    /// </summary>
    /// <seealso cref="MicroService.Models.MessageResult" />
    public class MessageResult
    {
        /// <summary>
        /// success flag
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// message code model
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        public int Code { get; set; }

        /// <summary>
        /// the return data
        /// </summary>
        public object Data { get; set; }

        /// <summary>
        /// total count
        /// </summary>
        public int TotalCount { get; set; } = 0;

        /// <summary>
        /// return successfully result
        /// </summary>
        /// <param name="data"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public static MessageResult SuccessResult(object data = null, int total = 0)
        {
            return new MessageResult
            {
                Success = true,
                Data = data,
                TotalCount = total,
                Message = MessageModel.GeneralSuccessfulMessage.Message,
                Code = MessageModel.GeneralSuccessfulMessage.Code
            };
        }

        /// <summary>
        /// return successfully result
        /// </summary>
        /// <returns></returns>
        public static MessageResult SuccessResult(MessageModel messageModel)
        {
            return new MessageResult
            {
                Success = true,
                Message = messageModel.Message,
                Code = messageModel.Code
            };
        }

        /// <summary>
        /// return successfully result
        /// </summary>
        /// <param name="data"></param>
        /// <param name="messageModel"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public static MessageResult SuccessResult(object data, MessageModel messageModel, int total = 0)
        {
            return new MessageResult
            {
                Success = true,
                Data = data,
                TotalCount = total,
                Message = messageModel.Message,
                Code = messageModel.Code
            };
        }

        /// <summary>
        /// return failure result
        /// </summary>
        /// <returns></returns>
        public static MessageResult FailureResult()
        {
            return new MessageResult
            {
                Success = false,
                Message = MessageModel.GeneralFailedMessage.Message,
                Code = MessageModel.GeneralFailedMessage.Code
            };
        }

        /// <summary>
        /// return failure result
        /// </summary>
        /// <returns></returns>
        public static MessageResult FailureResult(MessageModel messageModel)
        {
            return new MessageResult
            {
                Success = false,
                Message = messageModel.Message,
                Code = messageModel.Code
            };
        }
    }
}
