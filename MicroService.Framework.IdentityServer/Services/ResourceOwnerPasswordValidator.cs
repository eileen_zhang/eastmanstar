﻿using IdentityModel;

using IdentityServer4.Models;
using IdentityServer4.Validation;

using MicroService.Framework.IdentityServer.Services;
using MicroService.Models;
using MicroService.Models.Models;
using MicroService.Repository;

using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace MicroService.Framework.IdentityServer
{
    public class ResourceOwnerPasswordValidator : IResourceOwnerPasswordValidator
    {
        AuthService _authService;
        IRedisRepository _redisRepository;

        public ResourceOwnerPasswordValidator(AuthService authService, IRedisRepository redisRepository)
        {
            _authService = authService;
            _redisRepository = redisRepository;
        }

        /// <summary>
        /// check the user is validation, add uaername into claims
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            User user = _redisRepository.HashGet<User>(RedisKeys.Users, context.UserName);
            if (user != null)
            {
                if (!user.Password.Equals(context.Password, System.StringComparison.CurrentCultureIgnoreCase))
                {
                    user = null;
                }
            }
            else
            {
                user = await _authService.GetUser(context.UserName, context.Password);
                if (user != null)
                {
                    //set user entity to cache
                    _redisRepository.HashSet(RedisKeys.Users, user.Account, user);
                }
            }

            if (user != null)
            {
                context.Result = new GrantValidationResult(
                    subject: context.UserName,
                    authenticationMethod: OidcConstants.AuthenticationMethods.Password,
                    claims: new List<Claim>
                    {
                        new Claim(ClaimDefined.Account, context.UserName),
                    });
            }
            else
            {
                context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant, "invalid");
            }
        }
    }
}
