﻿using MicroService.BaseService.Controllers;
using MicroService.Framework.IdentityServer.Models;
using MicroService.Framework.IdentityServer.Models.Dtos;
using MicroService.Framework.IdentityServer.Models.Dtos.Auth;
using MicroService.Framework.IdentityServer.Services;
using MicroService.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Net;
using System.Threading.Tasks;

namespace MicroService.Framework.IdentityServer.Controllers
{
    /// <summary>
    /// auth login and logout contoller
    /// </summary>
    /// <seealso cref="MicroService.BaseService.Controllers.BaseAPIController" />
    /// <response code="90000">Operate successfully</response>
    /// <response code="99999">System exception</response>
    [ProducesResponseType(typeof(MessageResult<MessageModel>), 90000), ProducesResponseType(typeof(MessageResult<MessageModel>), 99999)]
    public class AuthController : BaseAPIController
    {
        ILogger<AuthController> _logger;
        AuthService _authService;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthController"/> class.
        /// </summary>
        /// <param name="logger">The logger.</param>
        /// <param name="authService">The authentication service.</param>
        public AuthController(ILogger<AuthController> logger, AuthService authService)
        {
            _logger = logger;
            _authService = authService;
        }

        /// <summary>
        /// Use account and pwd get token
        /// </summary>
        /// <param name="userLoginModel">The user login model.</param>
        /// <returns></returns>
        /// <response code="10003">err username or password</response>
        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(typeof(MessageResult<MessageModel>), 90000)]
        [ProducesResponseType(typeof(MessageResult<MessageModel>), 10003)]
        public async Task<IActionResult> BackendAuth([FromBody] UserLoginModel userLoginModel)
        {
            try
            {
                var token = await _authService.BackendAuth(userLoginModel.Account, userLoginModel.Password);
                return string.IsNullOrEmpty(token)
                    ? Ok(MessageResult.FailureResult(MessageModel.WrongAccountOrPasswordMessage))
                    : Ok(MessageResult.SuccessResult(token));
            }
            catch (Exception ex)
            {
                return Ok(MessageResult.FailureResult(new MessageModel(99999, ex.Message)));
            }
        }


        /// <summary>
        /// logout and Clear token
        /// </summary>
        /// <param name="input">token</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(typeof(MessageResult<MessageModel>), 90000)]
        public IActionResult Logout([FromBody] LogoutModel input)
        {
            try
            {
                var res = _authService.Logout(input.Token).Result;
                return Ok(MessageResult.SuccessResult(res));
            }
            catch (Exception ex)
            {
                return Ok(MessageResult.FailureResult(new MessageModel(9999, ex.Message)));
            }
        }



        /// <summary>
        /// Sends the reset mail.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>@MessageResult</returns>     
        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(typeof(MessageResult<MessageModel>), 90000)]
        public IActionResult SendResetMail([FromBody] AuthEmailInput input)
        {
            try
            {
                int res = _authService.SendResetPasswordEmail(input.AuthEmail).Result;

                return Ok(MessageResult.SuccessResult(res));
            }
            catch (Exception ex)
            {
                return Ok(MessageResult.FailureResult(new MessageModel(9999, ex.Message)));
            }
        }

        /// <summary>
        /// Verifies the email token.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        /// <response code="91000">err Email</response>
        [HttpGet]
        [AllowAnonymous]
        [ProducesResponseType(typeof(MessageResult<MessageModel>), 90000)]
        [ProducesResponseType(typeof(MessageResult<MessageModel>), 91000)]
        [ProducesResponseType(typeof(MessageResult<MessageModel>), 99999)]
        public IActionResult VerifyEmailToken([FromQuery] AuthEmailTokenInput input)
        {
            try
            {
                int res = _authService.VerifyEmailToken(input.AuthEmail, input.AuthEmailToken).Result;
                if (res == 1)
                {
                    return Ok(MessageResult.SuccessResult(res));
                }
                else
                {
                    return Ok(MessageResult.FailureResult(new MessageModel(91000, "")));
                }

            }
            catch (Exception ex)
            {
                return Ok(MessageResult.FailureResult(new MessageModel(99999, ex.Message)));
            }
        }

        /// <summary>
        /// Verifies the email token.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(typeof(MessageResult<MessageModel>), 90000)]
        public IActionResult UpdatePasswordByEmailToken([FromBody] AuthEmailTokenResetInput input)
        {
            try
            {
                int res = _authService.UpdatePasswordByEmailToken(input.AuthEmail, input.AuthEmailToken, input.ResetPassword).Result;
                return Ok(MessageResult.SuccessResult(res));
            }
            catch (Exception ex)
            {
                return Ok(MessageResult.FailureResult(new MessageModel(9999, ex.Message)));
            }
        }

        /// <summary>
        /// Minis the program authentication.
        /// </summary>
        /// <param name="wechatLoginModel">The wechat login model.</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(typeof(MessageResult<MessageModel>), 90000)]
        public MessageResult MiniProgramAuth(WechatLoginModel wechatLoginModel)
        {
            string url = $"https://api.weixin.qq.com/sns/jscode2session?appid=wx23c5a544c47b087c&secret=3d55ccc9618a2dda6099710f7363814d&js_code={wechatLoginModel.Code}&grant_type=authorization_code";
            try
            {
                string result = "";
                using (var client = new WebClient())
                {
                    result = client.DownloadString(url);
                }
                _logger.LogInformation(result);
                return MessageResult.SuccessResult(result);
            }
            catch (Exception ex)
            {
                return MessageResult.FailureResult();
            }
        }
    }
}
