﻿namespace MicroService.Models
{
    /// <summary>
    /// User Role Relation
    /// </summary>
    /// <seealso cref="BaseEntity{Int64}" />
    public class UserRole : BaseEntity<long>
    {
        /// <summary>
        /// Gets or sets the user identifier.
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        public long UserId { get; set; }

        /// <summary>
        /// Gets or sets the role identifier.
        /// </summary>
        /// <value>
        /// The role identifier.
        /// </value>
        public long RoleId { get; set; }
    }
}
