﻿using MicroService.BaseService.Controllers;
using MicroService.Models;
using MicroService.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Linq;

namespace TestService1.Controllers
{
    /// <summary>
    /// Test service home controller
    /// </summary>
    public class HomeController : BaseAPIController
    {
        ILogger<HomeController> _logger;
        IRedisRepository _redisRepository;

        public HomeController(ILogger<HomeController> logger, IRedisRepository redisRepository)
        {
            _logger = logger;
            _redisRepository = redisRepository;
        }

        /// <summary>
        /// haha
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Haha()
        {
            _logger.LogDebug("LogDebug");
            _logger.LogInformation("LogInformation");

            for (int i = 0; i < 100; i++)
            {
                _redisRepository.HashSet(RedisKeys.Users, "user" + i, new User { Id = i });
            }
            _redisRepository.KeyExpire(RedisKeys.Users, new TimeSpan(0, 10, 0));

            try
            {
                var a = 0;
                var b = 1000 / a;
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
            }
            var result = JsonConvert.SerializeObject(HttpContext.User.Claims.Select(a => new { a.Type, a.Value }));

            return Ok($"TestService1：{_redisRepository.HashGet(RedisKeys.Users, "user50")}");
        }

        /// <summary>
        /// need authentication
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult NeedsAuthentication()
        {
            return Ok($"NeedsAuthentication：{HttpContext.Request.Host.Port}");
        }

        /// <summary>
        /// Allow anonymous
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public IActionResult Descryt(UserInput user)
        {
            return Ok(MessageResult.SuccessResult(user));
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult Descryt1([FromBody] string user)
        {
            return Ok(MessageResult.SuccessResult(user));
        }
    }

    public class UserInput
    {
        public string Account { get; set; }

        public string Password { get; set; }
    }
}
