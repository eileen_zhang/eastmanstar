﻿using System.ComponentModel.DataAnnotations;

namespace MicroService.Models
{
    /// <summary>
    /// Role Entity
    /// </summary>
    /// <seealso cref="BaseEntity{Int64}" />
    public class Role : BaseEntity<long>
    {
        /// <summary>
        /// Role Name
        /// </summary>
        [StringLength(20)]
        public string RoleName { get; set; }

        /// <summary>
        /// Parent Role Id
        /// Dealing with hierarchical relationships between roles
        /// </summary>
        public bool IsDefault { get; set; }

        /// <summary>
        /// Description About Role
        /// </summary>
        [StringLength(100)]
        public string Description { get; set; }
    }
}
