﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace MicroService.BaseService.Migrations
{
    public partial class DeleteTable_Organization : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Organizations");

            migrationBuilder.UpdateData(
                table: "ConfigDictionarys",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedAt",
                value: new DateTime(2021, 3, 19, 17, 24, 56, 717, DateTimeKind.Local).AddTicks(6662));

            migrationBuilder.UpdateData(
                table: "ConfigDictionarys",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedAt",
                value: new DateTime(2021, 3, 19, 17, 24, 56, 717, DateTimeKind.Local).AddTicks(7546));

            migrationBuilder.UpdateData(
                table: "ConfigDictionarys",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreatedAt",
                value: new DateTime(2021, 3, 19, 17, 24, 56, 717, DateTimeKind.Local).AddTicks(7646));

            migrationBuilder.UpdateData(
                table: "ConfigDictionarys",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreatedAt",
                value: new DateTime(2021, 3, 19, 17, 24, 56, 717, DateTimeKind.Local).AddTicks(7673));

            migrationBuilder.UpdateData(
                table: "ConfigDictionarys",
                keyColumn: "Id",
                keyValue: 5L,
                column: "CreatedAt",
                value: new DateTime(2021, 3, 19, 17, 24, 56, 717, DateTimeKind.Local).AddTicks(7693));

            migrationBuilder.UpdateData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedAt",
                value: new DateTime(2021, 3, 19, 17, 24, 56, 717, DateTimeKind.Local).AddTicks(3593));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedAt",
                value: new DateTime(2021, 3, 19, 17, 24, 56, 715, DateTimeKind.Local).AddTicks(3896));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Organizations",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Address = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: false),
                    DeletedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeletedBy = table.Column<long>(type: "bigint", nullable: true),
                    FullName = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    Latitude = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    LegalPerson = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Longitude = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    ModifiedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ModifiedBy = table.Column<long>(type: "bigint", nullable: true),
                    ShortName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Telephone = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Organizations", x => x.Id);
                });

            migrationBuilder.UpdateData(
                table: "ConfigDictionarys",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedAt",
                value: new DateTime(2021, 3, 19, 15, 35, 25, 603, DateTimeKind.Local).AddTicks(71));

            migrationBuilder.UpdateData(
                table: "ConfigDictionarys",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedAt",
                value: new DateTime(2021, 3, 19, 15, 35, 25, 603, DateTimeKind.Local).AddTicks(1800));

            migrationBuilder.UpdateData(
                table: "ConfigDictionarys",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreatedAt",
                value: new DateTime(2021, 3, 19, 15, 35, 25, 603, DateTimeKind.Local).AddTicks(1975));

            migrationBuilder.UpdateData(
                table: "ConfigDictionarys",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreatedAt",
                value: new DateTime(2021, 3, 19, 15, 35, 25, 603, DateTimeKind.Local).AddTicks(2028));

            migrationBuilder.UpdateData(
                table: "ConfigDictionarys",
                keyColumn: "Id",
                keyValue: 5L,
                column: "CreatedAt",
                value: new DateTime(2021, 3, 19, 15, 35, 25, 603, DateTimeKind.Local).AddTicks(2072));

            migrationBuilder.UpdateData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedAt",
                value: new DateTime(2021, 3, 19, 15, 35, 25, 602, DateTimeKind.Local).AddTicks(4171));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedAt",
                value: new DateTime(2021, 3, 19, 15, 35, 25, 598, DateTimeKind.Local).AddTicks(8533));
        }
    }
}
