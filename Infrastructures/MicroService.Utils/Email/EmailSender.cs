﻿using System.Net;
using System.Net.Mail;

namespace MicroService.Utils.Email
{
    public class EmailSender
    {
        public void Send(Mail mail)
        {

            MailMessage mailMsg = new MailMessage
            {
                From = new MailAddress(mail.From, mail.FromDisplay),
                Subject = mail.MailTitle,
                Body = mail.MailBody,
                IsBodyHtml = mail.IsbodyHtml
            };

            foreach (var Recipient in mail.RecipientArry)
            {
                mailMsg.To.Add(new MailAddress(Recipient));
            }

            SmtpClient client = new SmtpClient
            {
                Host = "smtp.163.com",
                Port = 25,
                EnableSsl = true,
                Credentials = new NetworkCredential("gencore7400@163.com", "MPLGBLGLJXRHYSAR")
            };

            client.Send(mailMsg);

        }
    }
}
