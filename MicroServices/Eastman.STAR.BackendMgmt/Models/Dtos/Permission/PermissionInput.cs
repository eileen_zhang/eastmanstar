﻿using MicroService.Models;
using System.Collections.Generic;

namespace Eastman.STAR.BackendMgmt.Models.Dtos
{

    /// <summary>
    /// Permission  Model
    /// </summary>
    public class PermissionInput
    {

        /// <summary>
        /// Gets or sets the role identifier.
        /// </summary>
        /// <value>
        /// The role identifier.
        /// </value>
        public string RoleName { get; set; }


        /// <summary>
        /// Gets or sets the permissions.
        /// </summary>
        /// <value>
        /// The permissions.
        /// </value>
        public List<Permission> Permissions { get; set; }

    }
}
