﻿using MicroService.Utils;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace MicroService.BaseService.Middlewares
{
    /// <summary>
    /// Decrypt Post request data
    /// </summary>
    public class DecryptMiddleware
    {
        private readonly RequestDelegate _next;


        /// <summary>
        /// Initializes a new instance of the <see cref="DecryptMiddleware"/> class.
        /// </summary>
        /// <param name="next">The next.</param>
        public DecryptMiddleware(RequestDelegate next)
        {
            _next = next;
        }


        /// <summary>
        /// Invokes the specified HTTP context.
        /// </summary>
        /// <param name="httpContext">The HTTP context.</param>
        public async Task Invoke(HttpContext httpContext)
        {
            httpContext.Request.EnableBuffering();
            var request = httpContext.Request;
            if (request.Method.Equals("POST", StringComparison.CurrentCultureIgnoreCase))
            {
                if (!request.Path.Value.Equals("/connect/token", StringComparison.CurrentCultureIgnoreCase))
                {
                    var buffer = new byte[request.ContentLength ?? 0];
                    request.Body.Seek(0, System.IO.SeekOrigin.Begin);
                    await request.Body.ReadAsync(buffer, 0, buffer.Length);

                    var result = Encoding.UTF8.GetString(buffer);
                    var param = JsonConvert.DeserializeObject<PostParam>(result);

                    if (!string.IsNullOrEmpty(param.Param))
                    {
                        try
                        {
                            result = RSAHelper.Decrypt(param.Param, PostParam.PRIVATEKEY);
                        }
                        catch (Exception ex)
                        {
                        }
                        request.ContentLength = result.Length;
                        buffer = Encoding.UTF8.GetBytes(result);
                        var stream = new MemoryStream();
                        stream.Write(buffer, 0, buffer.Length);
                        stream.Seek(0, System.IO.SeekOrigin.Begin);
                        request.Body = stream;
                        request.Body.Position = 0;
                    }
                }
            }

            await _next(httpContext);
        }
    }
}
