﻿namespace Eastman.STAR.BackendMgmt.Models.Dtos
{
    /// <summary>
    /// update category or dictionary input
    /// </summary>
    public class UpdateDictionaryInput
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }
    }
}
