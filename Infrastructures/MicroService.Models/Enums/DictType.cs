﻿namespace MicroService.Models.Enums
{
    /// <summary>
    /// dictionary type
    /// </summary>
    public enum DictType
    {
        /// <summary>
        /// The type data
        /// </summary>
        CategoryData = 1001,

        /// <summary>
        /// The dictionary data
        /// </summary>
        DictionaryData = 1002,

        /// <summary>
        /// The value data
        /// </summary>
        ValueData = 1003
    }
}
