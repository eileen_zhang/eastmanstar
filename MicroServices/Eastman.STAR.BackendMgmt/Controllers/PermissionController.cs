﻿using Eastman.STAR.BackendMgmt.Models.Dtos;
using Eastman.STAR.BackendMgmt.Services;
using MicroService.BaseService.Controllers;
using MicroService.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Eastman.STAR.BackendMgmt.Controllers
{
    /// <summary>
    /// The Permission related
    /// </summary>
    /// <seealso cref="BaseAPIController" />
    /// <response code="90000">Operate successfully</response>
    /// <response code="90999">Operate failed</response>
    /// <response code="99999">System exception</response>
    [ProducesResponseType(90000),
          ProducesResponseType(90999),
          ProducesResponseType(99999)]
    public class PermissionController : BaseAPIController
    {
        PermissionServices _PermissionService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="PermissionService"></param>
        public PermissionController(PermissionServices PermissionService)
        {
            _PermissionService = PermissionService;
        }


        /// <summary>
        /// Gets the permission by role identifier.
        /// </summary>
        /// <param name="roleId">The role identifier.</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(List<Permission>), 90000)]
        public async Task<IActionResult> GetPermissionByRoleId(long roleId)
        {
            try
            {
                var Permissions = await _PermissionService.GetPermissionByRoleId(roleId);
                return Ok(MessageResult.SuccessResult(Permissions));
            }
            catch (Exception ex)
            {
                return Ok(MessageResult.FailureResult(new MessageModel(9999, ex.Message)));
            }
        }

        /// <summary>
        /// Gets all permission.
        /// </summary>
        /// <param name="roles">The roles.</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(List<Permission>), 90000)]
        public async Task<IActionResult> GetAllPermission([FromQuery] long[] roles)
        {
            try
            {
                var Permissions = await _PermissionService.GetAllPermission(roles);
                return Ok(MessageResult.SuccessResult(Permissions));
            }
            catch (Exception ex)
            {
                return Ok(MessageResult.FailureResult(new MessageModel(9999, ex.Message)));
            }
        }

        /// <summary>
        /// Saves the permission.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(MessageResult), 90000)]
        public async Task<IActionResult> SavePermission([FromBody] PermissionInput input)
        {
            try
            {
                var result = await _PermissionService.SavePermission(input);
                return result > 0 ? Ok(MessageResult.SuccessResult()) :
                                           Ok(MessageResult.FailureResult());
            }
            catch (Exception ex)
            {
                return Ok(MessageResult.FailureResult(new MessageModel(9999, ex.Message)));
            }
        }
    }
}
