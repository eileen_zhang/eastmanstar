﻿using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.Repository
{
    public interface IRepository<T> : IDisposable where T : class
    {
        /// <summary>
        /// 显式开启数据上下文事务
        /// </summary>
        /// <param name="isolationLevel">指定连接的事务锁定行为</param>
        void BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.Unspecified);

        ///// <summary>
        ///// 提交事务的更改
        ///// </summary>
        void Commit();

        ///// <summary>
        ///// 显式回滚事务，仅在显式开启事务后有用
        ///// </summary>
        void Rollback();

        void Add(T entity);

        void Modify(T entity);

        void Delete(T entity);
        /// <summary>
        /// 提交当前单元操作的更改
        /// </summary>
        int SaveChanges();



        Task<int> SaveChangesAsync();

        /// <summary>
        /// 获取 当前实体类型的查询数据集，数据将使用不跟踪变化的方式来查询，当数据用于展现时，推荐使用此数据集，如果用于新增，更新，删除时，请使用<see cref="TrackEntities"/>数据集
        /// </summary>
        IQueryable<T> Entities { get; }

        /// <summary>
        /// 获取 当前实体类型的查询数据集，当数据用于新增，更新，删除时，使用此数据集，如果数据用于展现，推荐使用<see cref="Entities"/>数据集
        /// </summary>
        IQueryable<T> TrackEntities { get; }

    }
}
