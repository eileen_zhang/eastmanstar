﻿namespace MicroService.Framework.IdentityServer.Models
{
    /// <summary>
    /// Used for wechat login
    /// </summary>
    public class WechatLoginModel
    {
        public string Code { get; set; }
    }
}
