﻿using AutoMapper;
using Eastman.STAR.BackendMgmt.Models.Bos;
using MicroService.BaseService.Models;
using MicroService.Models;
using MicroService.Models.Enums;
using MicroService.Repository;
using MicroService.Utils.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eastman.STAR.BackendMgmt.Services
{
    /// <summary>
    /// Dictionary Services
    /// </summary>
    public class DictionaryServices
    {
        readonly IRepository<ConfigDictionary> _repositoryDictionary;
        readonly IMapper _mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="DictionaryServices"/> class.
        /// </summary>
        /// <param name="repositoryDictionary">The repository dictionary.</param>
        /// <param name="mapper">The mapper.</param>
        public DictionaryServices(IRepository<ConfigDictionary> repositoryDictionary, IMapper mapper)
        {
            _repositoryDictionary = repositoryDictionary;
            _mapper = mapper;
        }

        /// <summary>
        /// Checks the dictionary value.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public Task<BaseResponseBo> CheckDictionaryValue(SaveDictionaryValueBo request)
        {
            //Code,ParentCode,Name,Desciprtion,Order,Id
            var result = Task.FromResult(new BaseResponseBo());
            if (string.IsNullOrEmpty(request.ParentCode))
            {
                result = Task.FromResult(new BaseResponseBo { Success = false, Message = MessageModel.DictionaryCodeIsEmptyMessage });
            }

            if (string.IsNullOrEmpty(request.Name))
            {
                result = Task.FromResult(new BaseResponseBo { Success = false, Message = MessageModel.DictionaryValueNameIsEmptyMessage });
            }

            if (string.IsNullOrEmpty(request.Code))
            {
                result = Task.FromResult(new BaseResponseBo
                {
                    Success = false,
                    Message = MessageModel.DictionaryValueCodeIsEmptyMessage
                });
            }

            if (!request.Code.CheckNumberOfContainsNumberAndLetter())
            {
                result = Task.FromResult(new BaseResponseBo { Success = false, Message = MessageModel.CodeNotNumbersOrLettersMessage });
            }


            if (!request.ParentCode.CheckNumberOfContainsNumberAndLetter())
            {
                result = Task.FromResult(new BaseResponseBo { Success = false, Message = MessageModel.CodeNotNumbersOrLettersMessage });
            }

            //Check ParentCode(Dictionary's Code)
            var dictionaryModel = _repositoryDictionary.Entities.FirstOrDefault(s => s.Code == request.ParentCode && s.DictionaryType == DictType.DictionaryData && !s.IsDeleted);
            if (dictionaryModel == null)
            {
                result = Task.FromResult(new BaseResponseBo { Success = false, Message = MessageModel.ParentCodeMustBeDictionaryCodeMessages });
            }

            return result;
        }

        /// <summary>
        /// Checks the code exist in add.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <returns></returns>
        public bool CheckCodeExistInAdd(string code)
        {
            bool result = false;
            var existCurrentCodeModel = _repositoryDictionary.Entities.FirstOrDefault(s => s.Code == code && !s.IsDeleted);
            if (existCurrentCodeModel != null)
            {
                result = true;
            }

            return result;
        }

        /// <summary>
        /// Checks the code exist in update.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <param name="code">The code.</param>
        /// <returns></returns>
        public bool CheckCodeExistInUpdate(long Id, string code)
        {
            bool result = false;
            var existCurrentCodeModel = _repositoryDictionary.Entities.FirstOrDefault(s => s.Id != Id && s.Code == code && !s.IsDeleted);
            if (existCurrentCodeModel != null)
            {
                result = true;
            }

            return result;
        }

        /// <summary>
        /// Adds the dictionary value.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="currentLoginUserId">The current login user identifier.</param>
        /// <returns></returns>
        public Task<BaseResponseBo<AddDictionaryValueOutputBo>> AddDictionaryValue(SaveDictionaryValueBo request, long currentLoginUserId)
        {
            if (request.Id != null && request.Id.Value > 0)
            {
                return new Task<BaseResponseBo<AddDictionaryValueOutputBo>>(() => new BaseResponseBo<AddDictionaryValueOutputBo> { Success = false, Message = MessageModel.InvalidIdMessage });
            }

            if (CheckCodeExistInAdd(request.Code))
            {
                return new Task<BaseResponseBo<AddDictionaryValueOutputBo>>(() => new BaseResponseBo<AddDictionaryValueOutputBo> { Success = false, Message = MessageModel.CodeMustBeUniqueMessage });
            }

            var existCurrentNameModel = _repositoryDictionary.Entities.FirstOrDefault(s => s.DictionaryType == DictType.ValueData && s.Name == request.Name && !s.IsDeleted);
            if (existCurrentNameModel != null)
            {
                return Task.FromResult(new BaseResponseBo<AddDictionaryValueOutputBo> { Success = false, Message = MessageModel.AddOrUpdateToBeAnExistedDictionaryValueName });
            }

            var addDictionaryValueModel = new ConfigDictionary();
            addDictionaryValueModel = _mapper.Map<ConfigDictionary>(request);
            addDictionaryValueModel.DictionaryType = DictType.ValueData;
            addDictionaryValueModel.CreatedAt = DateTime.Now;
            addDictionaryValueModel.CreatedBy = currentLoginUserId;

            _repositoryDictionary.Add(addDictionaryValueModel);
            var flag = _repositoryDictionary.SaveChangesAsync().Result;

            var responseBusinessModel = _mapper.Map<AddDictionaryValueOutputBo>(addDictionaryValueModel);

            return Task.FromResult(new BaseResponseBo<AddDictionaryValueOutputBo> { Data = responseBusinessModel, Success = flag > 0 });
        }

        /// <summary>
        /// Updates the dictionary value.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="currentLoginUserId">The current login user identifier.</param>
        /// <returns></returns>
        public Task<BaseResponseBo> UpdateDictionaryValue(SaveDictionaryValueBo request, long currentLoginUserId)
        {
            if (request.Id == null || request.Id.Value <= 0)
            {
                return new Task<BaseResponseBo>(() => new BaseResponseBo { Success = false, Message = MessageModel.InvalidIdMessage });
            }

            if (CheckCodeExistInUpdate(request.Id.Value, request.Code))
            {
                return new Task<BaseResponseBo>(() => new BaseResponseBo { Success = false, Message = MessageModel.CodeMustBeUniqueMessage });
            }

            var existCurrentNameModel = _repositoryDictionary.Entities.FirstOrDefault(s => s.Id != request.Id && s.Name == request.Name && !s.IsDeleted);
            if (existCurrentNameModel != null)
            {
                return Task.FromResult(new BaseResponseBo { Success = false, Message = MessageModel.AddOrUpdateToBeAnExistedDictionaryValueName });
            }

            var updateDictionaryValueModel = _repositoryDictionary.Entities.FirstOrDefault(s => s.Id == request.Id && s.DictionaryType == DictType.ValueData && !s.IsDeleted);
            if (updateDictionaryValueModel == null)
            {
                return Task.FromResult(new BaseResponseBo { Success = false, Message = MessageModel.IdOrAccountChangedMessage });
            }

            _mapper.Map(request, updateDictionaryValueModel);
            updateDictionaryValueModel.ModifiedAt = DateTime.Now;
            updateDictionaryValueModel.ModifiedBy = currentLoginUserId;

            _repositoryDictionary.Modify(updateDictionaryValueModel);
            return Task.FromResult(new BaseResponseBo { Success = _repositoryDictionary.SaveChangesAsync().Result > 0 });
        }

        /// <summary>
        /// Updates the category or dictionary.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="currentLoginUserId">The current login user identifier.</param>
        /// <returns></returns>
        public Task<BaseResponseBo> UpdateCategoryOrDictionary(UpdateDictionaryBo request, long currentLoginUserId)
        {
            if (request.Id <= 0)
            {
                return new Task<BaseResponseBo>(() => new BaseResponseBo { Success = false, Message = MessageModel.InvalidIdMessage });
            }

            if (string.IsNullOrEmpty(request.Name))
            {
                return new Task<BaseResponseBo>(() => new BaseResponseBo { Success = false, Message = MessageModel.CategoryOrDictionaryNameIsEmptyMessage });
            }

            var updateCategoryOrDictionaryModel = _repositoryDictionary.Entities.FirstOrDefault(s => s.Id == request.Id && s.DictionaryType != DictType.ValueData && !s.IsDeleted);

            _mapper.Map(request, updateCategoryOrDictionaryModel);
            updateCategoryOrDictionaryModel.ModifiedAt = DateTime.Now;
            updateCategoryOrDictionaryModel.ModifiedBy = currentLoginUserId;

            _repositoryDictionary.Modify(updateCategoryOrDictionaryModel);
            return Task.FromResult(new BaseResponseBo { Success = _repositoryDictionary.SaveChangesAsync().Result > 0 });
        }

        /// <summary>
        /// Deletes the dictionary value.
        /// </summary>
        /// <param name="dictionaryId">The dictionary identifier.</param>
        /// <param name="currentLoginUserId">The current login user identifier.</param>
        /// <returns></returns>
        public Task<BaseResponseBo> DeleteDictionaryValue(long dictionaryId, long currentLoginUserId)
        {
            if (dictionaryId <= 0)
            {
                return new Task<BaseResponseBo>(() => new BaseResponseBo { Success = false, Message = MessageModel.InvalidIdMessage });
            }

            var deleteDictionaryValueModel = _repositoryDictionary.Entities.FirstOrDefault(s => s.Id == dictionaryId && s.DictionaryType == DictType.ValueData && !s.IsDeleted);
            deleteDictionaryValueModel.IsDeleted = true;
            deleteDictionaryValueModel.DeletedAt = DateTime.Now;
            deleteDictionaryValueModel.DeletedBy = currentLoginUserId;

            _repositoryDictionary.Modify(deleteDictionaryValueModel);
            return Task.FromResult(new BaseResponseBo { Success = _repositoryDictionary.SaveChangesAsync().Result > 0 });
        }

        /// <summary>
        /// Gets the tree data.
        /// </summary>
        /// <returns></returns>
        public Task<List<GetTreeCategoriesOutputBo>> GetTreeData()
        {
            var categoryTreeMapperList = new List<GetTreeCategoriesOutputBo>();
            var cateogoryList = _repositoryDictionary.Entities.Where(s => !s.IsDeleted && s.DictionaryType == DictType.CategoryData).ToList();

            cateogoryList.ForEach(p => categoryTreeMapperList.Add(_mapper.Map<GetTreeCategoriesOutputBo>(p)));

            categoryTreeMapperList.ForEach(category =>
            {
                var dictionaryList = _repositoryDictionary.Entities.Where(s => !s.IsDeleted && s.DictionaryType == DictType.DictionaryData && s.ParentCode == category.Code).ToList();

                var dictionaryTreeMapperList = new List<GetTreeDictionariesOutputBo>();
                dictionaryList.ForEach(p => dictionaryTreeMapperList.Add(_mapper.Map<GetTreeDictionariesOutputBo>(p)));
                category.Dictionaries = dictionaryTreeMapperList;

                category.Dictionaries.ForEach(dictionary =>
                {
                    var dictionaryValueList = _repositoryDictionary.Entities.Where(s => !s.IsDeleted && s.DictionaryType == DictType.ValueData && s.ParentCode == dictionary.Code).ToList();
                    var dictionaryValueTreeMapperList = new List<GetTreeSubDictionariesOutputBo>();
                    dictionaryValueList.ForEach(p => dictionaryValueTreeMapperList.Add(_mapper.Map<GetTreeSubDictionariesOutputBo>(p)));
                    dictionaryValueTreeMapperList.OrderByDescending(s => s.Order);
                    dictionary.SubDictionaries = dictionaryValueTreeMapperList;
                });

            });
            return Task.FromResult(categoryTreeMapperList);
        }
    }
}
