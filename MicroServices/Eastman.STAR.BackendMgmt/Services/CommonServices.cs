﻿using Eastman.STAR.BackendMgmt.Models.Bos;
using MicroService.Models;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;

namespace Eastman.STAR.BackendMgmt.Services
{
    /// <summary>
    /// Common Services
    /// </summary>
    public class CommonServices
    {
        /// <summary>
        /// Gets the code message to list.
        /// </summary>
        /// <returns></returns>
        public Task<List<CodeMessageBo>> GetCodeMessageToList()
        {
            var result = new List<CodeMessageBo>();
            foreach (PropertyInfo property in typeof(MessageModel).GetProperties())
            {
                if (property.PropertyType == typeof(MessageModel))
                {
                    var obj = new MessageModel();
                    result.Add(new CodeMessageBo { Code = ((MessageModel)property.GetValue(obj)).Code, Message = ((MessageModel)property.GetValue(obj)).Message });
                }
            }
            return Task.FromResult(result);
        }
    }
}
