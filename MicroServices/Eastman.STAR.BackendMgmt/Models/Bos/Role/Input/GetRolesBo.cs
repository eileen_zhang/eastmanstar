﻿using MicroService.Models;

namespace Eastman.STAR.BackendMgmt.Models.Bos
{
    /// <summary>
    /// Select data transfer object of role table
    /// </summary>
    /// <seealso cref="PaginationRequest" />
    public class GetRolesBo : PaginationRequest
    {

        /// <summary>
        /// Gets or sets the prefix.
        /// </summary>
        /// <value>
        /// The prefix.
        /// </value>
        public string Prefix { get; set; }
    }
}
