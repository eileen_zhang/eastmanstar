﻿
using MicroService.Models;
using MicroService.Models.Models;
using MicroService.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;

namespace MicroService.BaseService.Controllers
{
    //[Authorize]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class BaseAPIController : ControllerBase
    {
        User _currentUser;

        public User CurrentUser
        {
            get
            {
                if (_currentUser != null)
                {
                    return _currentUser;
                }

                var redisRepository = HttpContext.RequestServices.GetService<IRedisRepository>();
                var claim = HttpContext.User.Claims.FirstOrDefault(a => a.Type == ClaimDefined.Account);
                //Get user from cache.
                _currentUser = redisRepository.HashGet<User>(RedisKeys.Users, claim.Value);
                //If cache has no user, get from db.
                if (_currentUser == null)
                {
                    var repository = HttpContext.RequestServices.GetService<IRepository<User>>();
                    _currentUser = repository.Entities.FirstOrDefault(a => a.Account == claim.Value);
                }

                return _currentUser;
            }
            set
            {
                _currentUser = value;
                var redisRepository = HttpContext.RequestServices.GetService<IRedisRepository>();
                redisRepository.HashSet(RedisKeys.Users, value.Account, value);
            }
        }

        //
        // 摘要:
        //     Creates an Microsoft.AspNetCore.Mvc.BadRequestObjectResult that produces a Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest
        //     response.
        //
        // 参数:
        //   error:
        //     An error object to be returned to the client.
        //
        // 返回结果:
        //     The created Microsoft.AspNetCore.Mvc.BadRequestObjectResult for the response.
        [NonAction]
        public override BadRequestObjectResult BadRequest([ActionResultObjectValue] object error)
        {
            return new BadRequestObjectResult(error);
        }
        //
        // 摘要:
        //     Creates an Microsoft.AspNetCore.Mvc.BadRequestObjectResult that produces a Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest
        //     response.
        //
        // 参数:
        //   modelState:
        //     The Microsoft.AspNetCore.Mvc.ModelBinding.ModelStateDictionary containing errors
        //     to be returned to the client.
        //
        // 返回结果:
        //     The created Microsoft.AspNetCore.Mvc.BadRequestObjectResult for the response.
        [NonAction]
        public override BadRequestObjectResult BadRequest([ActionResultObjectValue] ModelStateDictionary modelState)
        {
            return new BadRequestObjectResult(modelState);
        }

        public override BadRequestResult BadRequest()
        {
            var test = new ModelStateDictionary()
            {
            };

            return new BadRequestResult()
            {

            };
        }
    }
}
