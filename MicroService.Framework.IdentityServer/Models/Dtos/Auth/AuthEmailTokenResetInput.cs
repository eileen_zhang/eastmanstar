﻿namespace MicroService.Framework.IdentityServer.Models.Dtos.Auth
{
    public class AuthEmailTokenResetInput
    {
        /// <summary>
        /// Gets or sets the authentication email.
        /// </summary>
        /// <value>
        /// The authentication email.
        /// </value>
        public string AuthEmail { get; set; }

        /// <summary>
        /// Gets or sets the authentication email token.
        /// </summary>
        /// <value>
        /// The authentication email token.
        /// </value>
        public string AuthEmailToken { get; set; }

        /// <summary>
        /// Gets or sets the authentication email token.
        /// </summary>
        /// <value>
        /// The authentication email token.
        /// </value>
        public string ResetPassword { get; set; }
    }
}
