﻿using AutoMapper;
using Eastman.STAR.BackendMgmt.Models.Bos;
using Eastman.STAR.BackendMgmt.Models.Dtos;
using Eastman.STAR.BackendMgmt.Services;
using MicroService.BaseService.Controllers;
using MicroService.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Eastman.STAR.BackendMgmt.Controllers
{
    /// <summary>
    /// Dictionary Controller
    /// </summary>
    /// <seealso cref="BaseAPIController" />
    /// <response code="90000">Operate successfully</response>
    /// <response code="90999">Operate failed</response>
    /// <response code="99999">System exception</response>
    [ProducesResponseType(90000),
          ProducesResponseType(90999),
          ProducesResponseType(99999)]
    public class DictionaryController : BaseAPIController
    {
        private readonly DictionaryServices _dictionaryService;
        readonly IMapper _mapper;


        /// <summary>
        /// Initializes a new instance of the <see cref="DictionaryController" /> class.
        /// </summary>
        /// <param name="dictionaryService">The dictionary service.</param>
        /// <param name="mapper">The mapper.</param>
        public DictionaryController(DictionaryServices dictionaryService, IMapper mapper)
        {
            _dictionaryService = dictionaryService;
            _mapper = mapper;
        }

        /// <summary>
        /// Updates the category or dictionary name.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        /// <response code="10007">Id is invalid</response>
        /// <response code="90010">Category or dictionary name is empty</response>
        [ProducesResponseType(10007),
            ProducesResponseType(90010)]
        [HttpPost]
        public MessageResult UpdateCategory([FromBody] UpdateDictionaryInput request)
        {
            try
            {
                var updateCategoryMapperModel = _mapper.Map<UpdateDictionaryBo>(request);
                var responseResult = _dictionaryService.UpdateCategoryOrDictionary(updateCategoryMapperModel, CurrentUser.Id).Result;
                return responseResult.Success ? MessageResult.SuccessResult() :
                                       MessageResult.FailureResult(responseResult.Message);
            }
            catch (Exception ex)
            {
                return MessageResult.FailureResult(new MessageModel(9999, ex.Message));
            }
        }

        /// <summary>
        /// Adds the dictionary value.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        /// <response code="10007">Id is invalid</response>
        /// <response code="90011">Dictionary value code is empty</response>
        /// <response code="90012">Dictionary value name is empty</response>
        /// <response code="90013">Dictionary code is empty</response>
        /// <response code="90014">The code must contain numbers or letters</response>
        /// <response code="90015">Add or update to be an existed dictionary value name</response>
        /// <response code="90016">Code must be unique</response>
        /// <response code="90017">Parent code must be a data dictionary</response>
        [ProducesResponseType(10007),
            ProducesResponseType(90011),
            ProducesResponseType(90012),
            ProducesResponseType(90013),
            ProducesResponseType(90014),
            ProducesResponseType(90015),
            ProducesResponseType(90016),
            ProducesResponseType(90017)]
        [ProducesResponseType(typeof(MessageResult<AddDictionaryValueOutput>), 00000)]
        [HttpPost]
        public MessageResult AddDictionary([FromBody] SaveDictionaryValueInput request)
        {
            try
            {
                var addDictionaryMapperModel = _mapper.Map<SaveDictionaryValueBo>(request);
                var responseCheck = _dictionaryService.CheckDictionaryValue(addDictionaryMapperModel).Result;
                if (!responseCheck.Success)
                {
                    return MessageResult.FailureResult(responseCheck.Message);
                }

                var responseResult = _dictionaryService.AddDictionaryValue(addDictionaryMapperModel, CurrentUser.Id).Result;
                var addDictionaryResponseMapperModel = _mapper.Map<AddDictionaryValueOutput>(responseResult.Data);
                return responseResult.Success ? MessageResult.SuccessResult(addDictionaryResponseMapperModel) :
                                       MessageResult.FailureResult(responseResult.Message);
            }
            catch (Exception ex)
            {
                return MessageResult.FailureResult(new MessageModel(9999, ex.Message));
            }
        }

        /// <summary>
        /// Updates the dictionary value.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        /// <response code="10005">Id or account is changed</response>
        /// <response code="10007">Id is invalid</response>
        /// <response code="90011">Dictionary value code is empty</response>
        /// <response code="90012">Dictionary value name is empty</response>
        /// <response code="90013">Dictionary code is empty</response>
        /// <response code="90014">The code must contain numbers or letters</response>
        /// <response code="90015">Add or update to be an existed dictionary value name</response>
        /// <response code="90016">Code must be unique</response>
        /// <response code="90017">Parent code must be a data dictionary</response>
        [ProducesResponseType(10005),
            ProducesResponseType(10007),
            ProducesResponseType(90011),
            ProducesResponseType(90012),
            ProducesResponseType(90013),
            ProducesResponseType(90014),
            ProducesResponseType(90015),
            ProducesResponseType(90016),
            ProducesResponseType(90017)]
        [HttpPost]
        public MessageResult UpdateDictionary([FromBody] SaveDictionaryValueInput request)
        {
            try
            {
                var updateDictionaryMapperModel = _mapper.Map<SaveDictionaryValueBo>(request);
                var responseCheck = _dictionaryService.CheckDictionaryValue(updateDictionaryMapperModel).Result;
                if (!responseCheck.Success)
                {
                    return MessageResult.FailureResult(responseCheck.Message);
                }

                var responseResult = _dictionaryService.UpdateDictionaryValue(updateDictionaryMapperModel, CurrentUser.Id).Result;
                return responseResult.Success ? MessageResult.SuccessResult() :
                                       MessageResult.FailureResult(responseResult.Message);
            }
            catch (Exception ex)
            {
                return MessageResult.FailureResult(new MessageModel(9999, ex.Message));
            }
        }

        /// <summary>
        /// Deletes the dictionary.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        /// <response code="10007">Id is invalid</response>
        [ProducesResponseType(10007)]
        [HttpPost]
        public MessageResult DeleteDictionary([FromBody] IdInput request)
        {
            try
            {
                var responseResult = _dictionaryService.DeleteDictionaryValue(request.Id, CurrentUser.Id).Result;
                return responseResult.Success ? MessageResult.SuccessResult() :
                                      MessageResult.FailureResult();
            }
            catch (Exception ex)
            {
                return MessageResult.FailureResult(new MessageModel(9999, ex.Message));
            }
        }

        /// <summary>
        /// Gets the tree dictionaries include categories,dictionaries,values.
        /// </summary>
        /// <returns></returns>
        [ProducesResponseType(typeof(MessageResult<List<GetTreeCategoriesOutput>>), 00000)]
        [HttpGet]
        public MessageResult GetTreeDictionaries()
        {
            try
            {
                var responseResult = _dictionaryService.GetTreeData().Result;
                var dictionaryTreeMapperList = new List<GetTreeCategoriesOutput>();
                responseResult.ForEach(p => dictionaryTreeMapperList.Add(_mapper.Map<GetTreeCategoriesOutput>(p)));
                return MessageResult.SuccessResult(dictionaryTreeMapperList);
            }
            catch (Exception ex)
            {
                return MessageResult.FailureResult(new MessageModel(9999, ex.Message));
            }
        }
    }
}
