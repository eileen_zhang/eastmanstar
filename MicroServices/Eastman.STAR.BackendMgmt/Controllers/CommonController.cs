﻿using AutoMapper;
using Eastman.STAR.BackendMgmt.Models.Dtos;
using Eastman.STAR.BackendMgmt.Services;
using MicroService.BaseService.Controllers;
using MicroService.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Eastman.STAR.BackendMgmt.Controllers
{
    /// <summary>
    /// Common Controller
    /// </summary>
    /// <seealso cref="BaseAPIController" />
    public class CommonController : BaseAPIController
    {
        private readonly CommonServices _commonService;
        readonly IMapper _mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="CommonController"/> class.
        /// </summary>
        /// <param name="commonService">The common service.</param>
        /// <param name="mapper">The mapper.</param>
        public CommonController(CommonServices commonService, IMapper mapper)
        {
            _commonService = commonService;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets the code messages.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetCodeMessages()
        {
            try
            {
                var response_result = await _commonService.GetCodeMessageToList();
                var response_mapper_result = new List<CodeMessageOutput>();
                response_result.ForEach(p => response_mapper_result.Add(_mapper.Map<CodeMessageOutput>(p)));
                return Ok(MessageResult.SuccessResult(response_mapper_result));
            }
            catch (Exception ex)
            {
                return Ok(MessageResult.FailureResult(new MessageModel(9999, ex.Message)));
            }
        }
    }
}
