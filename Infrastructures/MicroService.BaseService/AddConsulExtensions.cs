﻿using Consul;
using MicroService.Models.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;

using System;

namespace MicroService.BaseService
{
    public static class AddConsulExtensions
    {
        //Add consul extenstion method
        public static void AddConsul(this IServiceCollection services)
        {
            var serviceProvider = services.BuildServiceProvider();
            var consulRegisterSettings = serviceProvider.GetService<IOptions<ConsulRegisterSettings>>().Value;
            var client = new ConsulClient(consulClientCfg =>
            {
                consulClientCfg.Address = new Uri(consulRegisterSettings.Address);
            });
            if (consulRegisterSettings.ServiceSettings == null)
            {
                return;
            }

            string serverId = consulRegisterSettings.ServiceSettings.Name + Guid.NewGuid();
            var result = client.Agent.ServiceRegister(new AgentServiceRegistration()
            {
                ID = serverId,
                Name = consulRegisterSettings.ServiceSettings.Name,
                Address = consulRegisterSettings.ServiceSettings.IP,
                Port = consulRegisterSettings.ServiceSettings.Port ?? 80,
                Check = new AgentServiceCheck //Health check
                {
                    DeregisterCriticalServiceAfter = TimeSpan.FromSeconds(5),//Deregister
                    Interval = TimeSpan.FromSeconds(10),
                    Timeout = TimeSpan.FromSeconds(5),
                    TCP = $"{consulRegisterSettings.ServiceSettings.IP}:{consulRegisterSettings.ServiceSettings.Port}"//Health check address.
                }
            });

            //Service Stop callback
            var lifetime = serviceProvider.GetService<IHostApplicationLifetime>();
            lifetime.ApplicationStopping.Register(() =>
            {
                Console.WriteLine("Deregister");
                client.Agent.ServiceDeregister(serverId).Wait();
            });
        }
    }
}
