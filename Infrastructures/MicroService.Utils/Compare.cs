﻿using System;
using System.Collections.Generic;

namespace MicroService.Utils
{
    /// <summary>
    /// Compare
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="C"></typeparam>
    public class Compare<T, C> : IEqualityComparer<T>
    {
        private readonly Func<T, C> _getField;

        /// <summary>
        /// Initializes a new instance of the <see cref="Compare{T, C}"/> class.
        /// </summary>
        /// <param name="getfield">The getfield.</param>
        public Compare(Func<T, C> getfield)
        {
            _getField = getfield;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bool Equals(T x, T y) => EqualityComparer<C>.Default.Equals(_getField(x), _getField(y));

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public int GetHashCode(T obj) => EqualityComparer<C>.Default.GetHashCode(_getField(obj));
    }
}
