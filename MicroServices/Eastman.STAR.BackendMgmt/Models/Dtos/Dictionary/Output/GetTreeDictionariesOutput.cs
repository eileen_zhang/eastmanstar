﻿using System.Collections.Generic;

namespace Eastman.STAR.BackendMgmt.Models.Dtos
{
    /// <summary>
    /// get tree dictionaries output
    /// </summary>
    public class GetTreeDictionariesOutput
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public long Id { get; set; }
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>
        /// The code.
        /// </value>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the sub dictionaries.
        /// </summary>
        /// <value>
        /// The sub dictionaries.
        /// </value>
        public List<GetTreeSubDictionariesOutput> SubDictionaries { get; set; }
    }
}
