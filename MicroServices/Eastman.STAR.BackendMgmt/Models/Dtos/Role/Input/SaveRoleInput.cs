﻿namespace Eastman.STAR.BackendMgmt.Models.Dtos
{
    /// <summary>
    /// Add and modify data transfer object of role table
    /// </summary>
    public class SaveRoleInput
    {
        /// <summary>
        /// Role Name
        /// </summary>
        public long? Id { get; set; }

        /// <summary>
        /// Role Name
        /// </summary>
        public string RoleName { get; set; }

        /// <summary>
        /// Description About Role
        /// </summary>
        public string Description { get; set; }
    }
}
