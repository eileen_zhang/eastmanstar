﻿using System.ComponentModel.DataAnnotations;

namespace MicroService.Framework.IdentityServer.Models
{
    /// <summary>
    /// The user login model
    /// </summary>
    public class UserLoginModel
    {
        /// <summary>
        /// Gets or sets the account.
        /// </summary>
        [Required(ErrorMessage = "123")]
        public string Account { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        public string Password { get; set; }
    }
}
