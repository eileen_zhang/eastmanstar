﻿namespace Eastman.STAR.BackendMgmt.Models.Dtos
{
    /// <summary>
    /// user output
    /// </summary>
    public class UserOutput
    {
        /// <summary>
        /// name
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// account
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// avatar
        /// </summary>
        public string Avatar { get; set; }

        /// <summary>
        /// roles
        /// </summary>
        public long[] Roles { get; set; }

        /// <summary>
        /// user introduction
        /// </summary>
        public string Introduction { get; set; }
    }

}
