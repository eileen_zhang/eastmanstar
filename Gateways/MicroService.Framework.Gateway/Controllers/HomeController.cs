﻿using Consul;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MicroService.Framework.Gateway.Controllers
{
    public class HomeController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public HomeController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Index()
        {
            return Ok("ok");
        }

        [HttpGet]
        [AllowAnonymous]
        public List<string> Services()
        {
            List<string> apis;
            var configuration = HttpContext.RequestServices.GetService(typeof(IConfiguration)) as IConfiguration;
            using (var consulClient = new Consul.ConsulClient(options => {
                options.Address = new Uri($"http://{_configuration["GlobalConfiguration:ServiceDiscoveryProvider:Host"]}:{_configuration["GlobalConfiguration:ServiceDiscoveryProvider:Port"]}");
            }))
            {
                var services = consulClient.Agent.Services().Result;
                apis = services.Response.Select(a =>$"{ a.Value.Service}({a.Value.Address}:{a.Value.Port})").Distinct().ToList();
            }

            return apis;
        }
    }
}
