﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace MicroService.Repository
{

    /// <summary>
    /// Repository extension
    /// </summary>
    public static class RepositoryExtension
    {
        /// <summary>
        /// Add db and inject repository and unitofwork
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static IServiceCollection AddDatabase(this IServiceCollection services)
        {
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddTransient(typeof(IRepository<>), typeof(Repository<>));

            return services;
        }

        /// <summary>
        /// Adds the redis.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <param name="redisOptions">The redis options.</param>
        /// <returns></returns>
        public static IServiceCollection AddRedis(this IServiceCollection services, Action<RedisOptions> redisOptions)
        {
            services.Configure<RedisOptions>(redisOptions);
            services.AddScoped<IRedisRepository, RedisRepository>();
            return services;
        }
    }
}
