﻿namespace Eastman.STAR.BackendMgmt.Models.Bos
{
    /// <summary>
    /// update category or dictionary business model
    /// </summary>
    public class UpdateDictionaryBo
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }
    }
}
