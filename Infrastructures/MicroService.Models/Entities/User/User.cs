﻿using System.ComponentModel.DataAnnotations;

namespace MicroService.Models
{
    /// <summary>
    /// User Entity
    /// </summary>
    /// <seealso cref="BaseEntity{Int64}" />
    public class User : BaseEntity<long>
	{
		/// <summary>
		/// Organization's pk
		/// </summary>
		public long OrganizationId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [MaxLength(255)]
		public string Name { get; set; }

        /// <summary>
        /// Gets or sets the account.
        /// </summary>
        /// <value>
        /// The account.
        /// </value>
        [MaxLength(32)]
		public string Account { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>
        /// The password.
        /// </value>
        [MaxLength(50)]
		public string Password { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>
        /// The email.
        /// </value>
        [MaxLength(100)]
		public string Email { get; set; }

        /// <summary>
        /// Gets or sets the phone.
        /// </summary>
        /// <value>
        /// The phone.
        /// </value>
        [MaxLength(20)]
		public string Phone { get; set; }
	}
}
