﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;

namespace MicroService.Repository
{
    public interface IRedisRepository
    {
        string GetString(RedisKeys key);
        T GetString<T>(RedisKeys key);
        long HashDelete(RedisKeys key, IEnumerable<string> hashFields);
        bool HashDelete(RedisKeys key, string hashField);
        bool HashExists(RedisKeys key, string hashField);
        IEnumerable<string> HashGet(RedisKeys key, IEnumerable<string> hashFields);
        string HashGet(RedisKeys key, string hashField);
        T HashGet<T>(RedisKeys key, string hashField);
        IEnumerable<string> HashKeys(RedisKeys key);
        void HashSet(RedisKeys key, IEnumerable<KeyValuePair<string, string>> hashFields);
        bool HashSet(RedisKeys key, string hashField, string value);
        bool HashSet<T>(RedisKeys key, string hashField, T value);
        IEnumerable<string> HashValues(RedisKeys key);
        long KeyDelete(IEnumerable<RedisKeys> keys);
        bool KeyDelete(RedisKeys key);
        bool KeyExists(RedisKeys key);
        bool KeyExpire(RedisKeys key, TimeSpan? expiry);
        string ListLeftPop(RedisKeys key);
        T ListLeftPop<T>(RedisKeys key);
        long ListLeftPush(RedisKeys key, string value);
        long ListLeftPush<T>(RedisKeys key, T value);
        long ListLength(RedisKeys key);
        IEnumerable<string> ListRange(RedisKeys key, long start = 0, long stop = -1);
        long ListRemove(RedisKeys key, string value);
        string ListRightPop(RedisKeys key);
        T ListRightPop<T>(RedisKeys key);
        long ListRightPush(RedisKeys key, string value);
        long ListRightPush<T>(RedisKeys key, T value);
        bool SetString(RedisKeys key, string value, TimeSpan? expiry = null);
        bool SetString<T>(RedisKeys key, T value, TimeSpan? expiry = null);
        bool SortedSetAdd(RedisKeys key, string member, double score);
        bool SortedSetAdd<T>(RedisKeys key, T member, double score);
        double SortedSetIncrement(RedisKeys key, string member, double value = 1);
        long SortedSetLength(RedisKeys key);
        bool SortedSetLength(RedisKeys key, string memebr);
        IEnumerable<string> SortedSetRangeByRank(RedisKeys key, long start = 0, long stop = -1, Order order = Order.Ascending);
    }
}