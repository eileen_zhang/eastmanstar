﻿namespace MicroService.Framework.IdentityServer.Models.Dtos.Auth
{
    /// <summary>
    ///  AuthEmailInput Dto
    /// </summary>
    public class AuthEmailInput
    {
        /// <summary>
        /// Gets or sets the authentication email.
        /// </summary>
        /// <value>
        /// The authentication email.
        /// </value>
        public string AuthEmail { get; set; }
    }
}
