﻿using System;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace MicroService.Repository
{
    public interface IUnitOfWork : IDisposable
    {
        void BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.Unspecified);

        void Commit();

        void Rollback();

        /// <summary>
        /// do save async
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        Task<int> SaveChangesAsync(CancellationToken token = default(CancellationToken));

        /// <summary>
        /// do save
        /// </summary>
        /// <returns></returns>
        int SaveChanges();

        IRepository<T> GetStore<T>() where T : class;
    }
}
