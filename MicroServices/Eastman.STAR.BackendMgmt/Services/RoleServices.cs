﻿using AutoMapper;
using Eastman.STAR.BackendMgmt.Models.Bos;
using MicroService.BaseService.Models;
using MicroService.Models;
using MicroService.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eastman.STAR.BackendMgmt.Services
{
    /// <summary>
    /// Role Services
    /// </summary>
    public class RoleServices
    {
        readonly IRepository<Role> _repositoryRole;
        readonly IRepository<User> _repositoryUser;
        readonly IMapper _mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="RoleServices"/> class.
        /// </summary>
        /// <param name="repositoryRole">The repository role.</param>
        /// <param name="repositoryUser">The repository user.</param>
        /// <param name="mapper">The mapper.</param>
        public RoleServices(IRepository<Role> repositoryRole, IRepository<User> repositoryUser, IMapper mapper)
        {
            _repositoryRole = repositoryRole;
            _repositoryUser = repositoryUser;
            _mapper = mapper;
        }

        /// <summary>
        /// Get Roles
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public Task<List<RoleOutputBo>> GetRoles(GetRolesBo request)
        {
            var result = _repositoryRole.Entities.Where(s => !s.IsDeleted).ToList();
            if (!string.IsNullOrEmpty(request.Prefix))
            {
                result = result.Where(s => s.RoleName.Contains(request.Prefix) || s.Description.Contains(request.Prefix)).ToList();
            }

            request.TotalCount = result.Count;
            result = result.Skip(request.PageIndex * request.PageSize).Take(request.PageSize).ToList();

            var roleMapperList = new List<RoleOutputBo>();
            result.ForEach(p => roleMapperList.Add(_mapper.Map<RoleOutputBo>(p)));
            roleMapperList.ForEach(s => s.CreatedUser = _repositoryUser.Entities.FirstOrDefault(u => u.CreatedBy == s.CreatedBy && !u.IsDeleted)?.Name);
            return Task.FromResult(roleMapperList);
        }

        /// <summary>
        /// Get Single Role By Role Id
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public Task<RoleOutputBo> GetRole(long roleId)
        {
            var result = _repositoryRole.Entities.FirstOrDefault(s => s.Id == roleId && !s.IsDeleted);
            var roleMapperModel = _mapper.Map<RoleOutputBo>(result);
            roleMapperModel.CreatedUser = _repositoryUser.Entities.FirstOrDefault(u => u.CreatedBy == roleMapperModel.CreatedBy && !u.IsDeleted)?.Name;
            return Task.FromResult(roleMapperModel);
        }

        /// <summary>
        /// Add Role
        /// </summary>
        /// <param name="request"></param>
        /// <param name="currentLoginUserId"></param>
        /// <returns></returns>
        public Task<BaseResponseBo> AddRole(SaveRoleBo request, long currentLoginUserId)
        {
            if (request.Id != null && request.Id.Value > 0)
            {
                return new Task<BaseResponseBo>(() => new BaseResponseBo { Success = false, Message = MessageModel.InvalidIdMessage });
            }

            var existRoleNameModel = _repositoryRole.Entities.FirstOrDefault(s => s.RoleName == request.RoleName && !s.IsDeleted);
            if (existRoleNameModel != null)
            {
                return Task.FromResult(new BaseResponseBo { Success = false, Message = MessageModel.AddOrUpdateToBeAnExistedRoleName });
            }
            var addRoleModel = new Role();
            addRoleModel = _mapper.Map<Role>(request);
            addRoleModel.CreatedAt = DateTime.Now;
            addRoleModel.CreatedBy = currentLoginUserId;

            _repositoryRole.Add(addRoleModel);
            return Task.FromResult(new BaseResponseBo { Success = _repositoryRole.SaveChangesAsync().Result > 0 });
        }

        /// <summary>
        /// Update Role
        /// </summary>
        /// <param name="request"></param>
        /// <param name="currentLoginUserId"></param>
        /// <returns></returns>
        public Task<BaseResponseBo> UpdateRole(SaveRoleBo request, long currentLoginUserId)
        {
            if (request.Id == null || request.Id.Value <= 0)
            {
                return new Task<BaseResponseBo>(() => new BaseResponseBo { Success = false, Message = MessageModel.InvalidIdMessage });
            }

            var existRoleNameModel = _repositoryRole.Entities.FirstOrDefault(s => s.Id != request.Id && s.RoleName == request.RoleName && !s.IsDeleted);
            if (existRoleNameModel != null)
            {
                return Task.FromResult(new BaseResponseBo { Success = false, Message = MessageModel.AddOrUpdateToBeAnExistedRoleName });
            }

            var updateRoleModel = _repositoryRole.Entities.FirstOrDefault(s => s.Id == request.Id && !s.IsDeleted);
            if (updateRoleModel == null)
            {
                return Task.FromResult(new BaseResponseBo { Success = false, Message = MessageModel.IdOrAccountChangedMessage });
            }

            _mapper.Map(request, updateRoleModel);
            updateRoleModel.ModifiedAt = DateTime.Now;
            updateRoleModel.ModifiedBy = currentLoginUserId;

            _repositoryRole.Modify(updateRoleModel);
            return Task.FromResult(new BaseResponseBo { Success = _repositoryRole.SaveChangesAsync().Result > 0 });
        }

        /// <summary>
        /// Delete Role
        /// </summary>
        /// <param name="roleId"></param>
        /// <param name="currentLoginUserId"></param>
        /// <returns></returns>
        public Task<BaseResponseBo> DeleteRole(long roleId, long currentLoginUserId)
        {
            if (roleId <= 0)
            {
                return new Task<BaseResponseBo>(() => new BaseResponseBo { Success = false, Message = MessageModel.InvalidIdMessage });
            }

            var deleteRoleModel = _repositoryRole.Entities.FirstOrDefault(s => s.Id == roleId && !s.IsDeleted);
            deleteRoleModel.IsDeleted = true;
            deleteRoleModel.DeletedAt = DateTime.Now;
            deleteRoleModel.DeletedBy = currentLoginUserId;

            _repositoryRole.Modify(deleteRoleModel);
            return Task.FromResult(new BaseResponseBo { Success = _repositoryRole.SaveChangesAsync().Result > 0 });
        }

        /// <summary>
        /// Check Role Name Is Empty
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Task<BaseResponseBo> CheckRole(SaveRoleBo request)
        {
            return string.IsNullOrEmpty(request.RoleName)
                ? Task.FromResult(new BaseResponseBo { Success = false, Message = MessageModel.RoleNameIsEmptyMessage })
                : Task.FromResult(new BaseResponseBo());
        }
    }
}
