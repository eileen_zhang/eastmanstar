﻿using MicroService.Models;
using MicroService.Utils;

using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.BaseService.Context.Configurations
{
	public class EntityCfg
	{
		public static void EntitySettings(ModelBuilder modelBuilder)
		{
			//set filter
			modelBuilder.Entity<User>().HasQueryFilter(a => !a.IsDeleted);
			modelBuilder.Entity<Permission>().HasQueryFilter(a => !a.IsDeleted);
            modelBuilder.Entity<ConfigDictionary>().HasQueryFilter(a => !a.IsDeleted);
			//modelBuilder.Entity<User>().Property(a => a.Id).ValueGeneratedNever();
		}

		public static void InitializeDatas(ModelBuilder modelBuilder)
		{
			//set default data
			modelBuilder.Entity<User>().HasData(new User { Id = 1, Name = "Administrator", Account = "admin", Password = MD5Helper.Md5("123456"), CreatedAt = DateTime.Now, CreatedBy = 1 });

            modelBuilder.Entity<Role>().HasData(new Role { Id = 1, RoleName = "Administrator", IsDefault = true, Description = "System default administrator role", CreatedAt = DateTime.Now, CreatedBy = 1 });

            modelBuilder.Entity<ConfigDictionary>().HasData(new ConfigDictionary { Id = 1, DictionaryType = MicroService.Models.Enums.DictType.CategoryData, Name = "基本分类", Code = "BASIC", Description = "default category", CreatedAt = DateTime.Now, CreatedBy = 1 });

            modelBuilder.Entity<ConfigDictionary>().HasData(new ConfigDictionary { Id = 2, DictionaryType = MicroService.Models.Enums.DictType.DictionaryData, Name = "地区", Code = "AREA", ParentCode = "BASIC", Description = "default dictionary about area", CreatedAt = DateTime.Now, CreatedBy = 1 });

            modelBuilder.Entity<ConfigDictionary>().HasData(new ConfigDictionary { Id = 3, DictionaryType = MicroService.Models.Enums.DictType.DictionaryData, Name = "性别", Code = "SEX", ParentCode = "BASIC", Description = "default dictionary about sex", CreatedAt = DateTime.Now, CreatedBy = 1 });

            modelBuilder.Entity<ConfigDictionary>().HasData(new ConfigDictionary { Id = 4, DictionaryType = MicroService.Models.Enums.DictType.ValueData, Name = "男", Code = "MALE", ParentCode = "SEX", Description = "default dictionary about sex", CreatedAt = DateTime.Now, CreatedBy = 1 });

            modelBuilder.Entity<ConfigDictionary>().HasData(new ConfigDictionary { Id = 5, DictionaryType = MicroService.Models.Enums.DictType.ValueData, Name = "女", Code = "FEMALE", ParentCode = "SEX", Description = "default dictionary about sex", CreatedAt = DateTime.Now, CreatedBy = 1 });
        }
	}
}
